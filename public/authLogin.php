<?php


session_start();
require_once(__DIR__."/../controller/vendor/autoload.php");



$Page = new Page();
$User = new User();
$Redirect = new Redirect();


if ($User->isLoggedIn()) {
    $Redirect->to404();
}


$Page->render("authForm", "2 Factor Authentication", "Submit 2FA for login security");

?>