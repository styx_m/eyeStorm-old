<?php

session_start();

require_once(__DIR__."/../controller/vendor/autoload.php");


    $Page = new Page();
    $User = new User();

    $token = $User->getJWT();

    if ($User->isLoggedIn() || isset($token->id)) { // id is specifically mentioned since 2fa
        $User->logout();
    }

    $Page->render("stormLogin", "Login", "Login Screen for TABCEOTS");
 ?>
