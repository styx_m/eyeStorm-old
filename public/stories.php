<?php 

session_start();

require_once(__DIR__."/../controller/vendor/autoload.php");





$Val = new Validate();
$Redirect = new Redirect();
$Article = new Article();

$Page = new Page();

$filteredGet = $Val->filterArgs($_GET);

if (!isset($_COOKIE["pagesViewed"])) {
    setcookie('pagesViewed', json_encode([]), null, "/");
}

if (isset($filteredGet["story"]) && !empty($filteredGet["story"]) && $filteredGet["story"] !== "/") {

    $storyName = $filteredGet["story"];


    if (!$Article->exists($filteredGet["issue"], rawurlencode($storyName))) {
        $Redirect->to404();
        exit;
    }

    $Page->render("story", $storyName, "Story about " . $storyName);
    exit;

}

?>

