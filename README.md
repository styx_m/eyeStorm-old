# eyeStorm
Eye of the Storm  Newpaper

<a href="https://github.com/DovidM/eyeStorm.git">This</a> is the source code behind https://tabceots.com


<h3> Build Instructions </h3>

<p>
To build, download the code, then point your webroot at the public directory.
You also must have a mysql database on hand. To connect, create a file called <code>config.php</code> 
in the <code>controller</code> directory and paste this into it (substituting the 2nd parameter for your own settings)
</p>

<code>
  define("DB_USER", "YOUR_USERNAME");
  define("DB_PASS", "YOUR_PASSWORD");
  define("DB_HOST", "YOUR_HOST");
  define("DB_NAME", "YOUR_DB_NAME");
</code>

<p>
To get email set up, add the following to the config file.
</p>

<code>
  define ("EMAIL_ADDR", "example@domain.ext");
  define ("EMAIL_PASS", "my secure password");
  define ("EMAIL_HOST", "smtp.domain.com");
  define ("EMAIL_NAME", "My wonderful newpaper");
  define ("EMAIL_PORT", ); // put whatever port number you're using
</code>

<p>
To set up JSON web token, add the following to the config file:
</p>
<code>
    define("JWT_SECRET", "mySecureSecret");
</code>

<p>Information regarding database setup can be found in controller/db.php</p>


<h3>Adding Tags</h3>
<p>
To add tags, change <code>views/publishForm</code> and <code>views/templates/menuBar</code>.
    Once that is done, other places are updated (and lvl 3 can choose to change to that tag once at least 1 article already has it.
</p>

<h3> What users can do</h3>

<ul>
<li>All people can view published issues/article, profiles of people who have an account, comments on articles, and the mission statement.</li>

<li>Users with an account can create articles, edit their own articles before the issue is published, delete own articles. They can also see articles from an issue that isn't published yet and comment on any articles.</li>

<li>Level 2 users can do everything level 1 users can, plus delete level 1 users, promote a user to level 2, and create accounts that are level 2.</li>

<li>Level 3 users can do everything level 2 can do, plus edit any articles (even if not created by that user) as long as the issue is not published. They can also delete any article at any time, delete any comments, delete any user except other level 3s, promote any user to level 2 or 3, demote level 2 users to level 1. They can also publish an issue (so that the articles in the issue are viewable to everyone on the internet), and create a name for the issue.</li>
</ul>

<h3>Misc</h3>

<ul>
<li>(Real) Files (as opposed to files that are saved to db) that an admin can edit directly:
    <code>views/missionView</code></li>
<li>All emails must end in @tabc.org. If you're using this project for something else, search for all instances of @tabc.org and replace with whatever you want.</li>
</ul>

