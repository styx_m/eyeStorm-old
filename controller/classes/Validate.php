<?php

require_once(__DIR__ . "/../vendor/autoload.php");


// validates input
Class Validate {

    /**
      * @param $argsExpected - results of func_get_args()
      * @param $numOfParamsWanted - number
      *
      * @return true $numOfParamsWanted >= $argsExpected and no empty or null $argsExpected
      *    Else shows user error message, exits
      */
    public function allArgsFilled(array $argsExpected, int $numOfParamsWanted) {

        $Page = new Page();

        $filteredArgs = [];

        if (sizeof($argsExpected) < $numOfParamsWanted) {
            $Page->displayMessage();
            exit;
        }

        foreach ($argsExpected as $arg) {

            if (!isset($arg) || empty($arg)) {

                $Page->displayMessage("error", "empty");
                exit;
            }
        }

        return true;

    }

    /**
      * @param $args - array
      * @param $names - If want to give different keys to values in array, $names should be array of the new keys
      *    If it's false (as it must be for muultidimentional arrays), regular 0 based indexes used instead
      *
      * @return $args which have been sanitized using FILTER_SANITIZE_STRING
      */
    public function filterArgs(array $args = [], array $names = []) {

        $filteredArgs = [];

        $i = 0;

        foreach ($args as $key => $arg) {

            $arrKey = (!empty($names[$i])) ? $names[$i] : $key;

            $arrKey = ($names === false) ? $i : $arrKey;

            if (is_array($arg)) {

              $nameArg = (is_array($key)) ? $key : [];

              $filteredArgs[$arrKey] = $this->filterArgs($arg, $nameArg);
              continue;
            }

            $filteredArgs[$arrKey] = filter_var($arg, FILTER_SANITIZE_STRING);
            $i++;
        }

        return $filteredArgs;
    }

}