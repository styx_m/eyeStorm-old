<?php


class Redirect {

    private $message;


    public function setMessage(string $type = "", string $message = "") {

        $this->message = ($type) ? "?{$type}={$message}" : "";
    }

    public function toSamePage(string $type = "", string $message = "") {

        if ($type) {
            $this->setMessage($type, $message);
        }


        $_SERVER["HTTP_REFERER"] = preg_replace('/\?.*/', '', $_SERVER["HTTP_REFERER"]);

        $this->switchTo($_SERVER["HTTP_REFERER"]);
    }

    public function toTwoFactor() {

        $this->switchTo("/authLogin");
    }

    public function toLogin() {
        $this->switchTo("/login");
    }

    public function to404() {
        $this->switchTo("/404");
    }

    public function to401() {
        $this->switchTo("/forbidden");
    }

    public function toPasswordRecovery() {
        $this->switchTo("/forgotPass");
    }

    public function toHome() {
        $this->switchTo("/");
    }

    public function toIssues() {
        $this->switchTo("/issue");
    }

    public function toMission() {
        $this->switchTo("/mission");
    }

    public function toArticleSettings() {
        $this->switchTo("/modifyArticles");
    }

    public function toUserSettings() {
        $this->switchTo("/modifyUsers");
    }

    public function toPublish() {
        $this->switchTo("/publish");
    }

    public function toSignup() {
        $this->switchTo("/signup");
    }

    public function toStories() {
        $this->switchTo("/stories");
    }

    public function toUserInfo() {
        $this->switchTo("/u");
    }


        /**
      * @author - Harvard's CS50
      *
      * @param $location - page to switchTo user to
      */
    private function switchTo(string $location) {

        if (headers_sent($file, $line))
        {
            trigger_error("HTTP headers already sent at {$file}:{$line}", E_USER_ERROR);
        }

        header("Location: {$location}{$this->message}");

        exit;
    }
}



?>