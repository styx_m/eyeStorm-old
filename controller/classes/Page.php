<?php

require_once(__DIR__."/../vendor/autoload.php");

$Page = new Page();
$Page->restrictAccess(); // every time a page loads, this runs


class Page {

    /**
      * @param $page - name of valid file in the views/ directory to serve
      * @param $title - title of page meta tag info, seen in browser tabs
      * @param $description - meta tag info
      * @param $story - if true, $page gets echoed out instead of requiring it
      *
      * @return displays page passed in to user
      */
    public function render(string $page, string $title = "STORM News", string $description = "Eye Of The Storm") {

        require_once("../views/templates/head.php");
        require_once("../views/templates/menuBar.php");

        require_once("../views/" . $page . ".html");

        require_once("../views/templates/foot.php");
    }

    /**
      * @author - Harvard's CS50
      *
      * @param $location - page to redirect user to
      */
    function redirect(string $location) {

        if (headers_sent($file, $line))
        {
            trigger_error("HTTP headers already sent at {$file}:{$line}", E_USER_ERROR);
        }

        header("Location: {$location}");

        exit;
    }

    /**
      * <p>
      * Redirects user to same page as already on, but with a query string consisting of $key = $value
      * </p>
      *
      * @param $key - key of query string
      * @param $value - value of query string
      */
    public function displayMessage(string $key = "error", string $value = "fail") {


        $_SERVER["HTTP_REFERER"] = preg_replace('/\?.*/', '', $_SERVER["HTTP_REFERER"]);

        $this->redirect($_SERVER["HTTP_REFERER"] . "?{$key}={$value}");
        exit;
    }


    /**
      * <p>
      * Called immediately after, every time a page loads, checks if user has authority to be in paage
      *   If not, user is shown error page
      * Also revalidates user's level by calling $this->checkLvl and calls $this->checkSessionAge
      * </p>
      *
      * @return true if user can access page, else false
      */
    function restrictAccess() {

        $User = new User();
        $token = $User->getJWT();

        $this->checkLvl();

        $this->checkSessionAge();

        $restrictedAccess = ['logout', 'publish', 'modifyarticles', 'modifyusers'];

        $requested = filter_var($_SERVER["REQUEST_URI"], FILTER_SANITIZE_URL);

        $requested = strtolower($requested);

        preg_match("/[a-zA-Z1-9_-]+/", $requested, $match);

        // when user less than lvl 3 goes to modifyarticles only can see own
        $perms = ['logout' => 1, 'publish' => 1, 'modifyusers' => 2, 'modifyarticles' => 3];

        $match = isset($match[0]) ? $match[0] : "";

        if (in_array($match, $restrictedAccess)) {

            if (!$User->isLoggedIn() || !isset($token->level) || ($token->level < $perms[$match] && $_SERVER["REQUEST_METHOD"] == "GET")) {

                $this->redirect("/forbidden");
                return false;
            }
        }

        return true;
    }

    /**
      * <p>
      * Renews user's level in case of change
      * Called every time page loads by $this->restrictAccess
      * </p>
      */
    private function checkLvl() {

        $User = new User();
        $token = $User->getJWT();

        // extra security; if a user gets deleted while logged in, can't do anything
        if (isset($token->level)) {
            $db = new MyDB();

            $lvl = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE ID = ?", $token->id);

            $userLvl = $lvl->fetchColumn();

            $User->changeJWT(["level" => isset($userLvl) ? $userLvl : 0]);
        }
    }


    /**
      * <p>
      * If user inactive for 15 minutes, logged out
      * </p>
      */
    private function checkSessionAge() {

        $User = new User();
        $token = $User->getJWTHeaders();

        if ($User->isLoggedIn() && isset($token->iat) && time() - $token->iat > 900) {
            $User->logout();
        }
        else {

            $User->changeJWT(); // not changing payload, but headers still change
        }
    }

}


?>