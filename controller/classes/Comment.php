<?php



require_once(__DIR__."/../vendor/autoload.php");

/**
  * Represents a comment to an article
  * RI: Comments must be at most 500 chars and at least 10
  * This is an immutable datatype (can't change except for deletion)
  */
class Comment {

    private $id, $content, $artId, $authorId, $created;


    /**
      * Fills instance variables with info about comment whose id is $commentId
      *
      * @param $commentId - id of comment
      */
    public function defineInfo(int $commentId) {

        $db = new MyDB();

        $infoQuery = $db->catchMistakes("SELECT ID, CONTENT, ART_ID, AUTHORID, CREATED FROM COMMENTS WHERE ID = ?", $commentId);
        $infoQuery->setFetchMode(PDO::FETCH_NUM);

        list($this->id, $this->content, $this->artId, $this->authorId, $this->created) = $infoQuery->fetchAll()[0];
    }

    public function route(string $method, array $data) {

        switch($method) {

            case "GET":
                return $this->getAllByArticle($data["issue"], $data["url"]);

            case "POST":
                return $this->create($data["issue"], $data["url"], $data["content"]);

            case "DELETE":
                $this->defineInfo($data["id"]);
                return $this->delete();
        }
    }


    /**
    * Creates a comment. Requires user to be logged in
    *
    * @param $issue - issue of article
    * @param $url - url of article
    * @param $text - text of comment. Must be at least 10 chars (magic num) and <= 500 chars
    * @return id if saved successfully, else false
    */
    public function create(int $issue, string $url, string $text) {

        $User = new User();
        $db = new MyDB();
        $token = $User->getJWT();

        $filteredText = $this->stripTags($text);

        $artId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? AND URL = ?", [$issue, $url])->fetchColumn();

        if (!$User->isLoggedIn()) {
            Utilities::setHeader(401);
            return false;
        }

        if (strlen($filteredText) < 4 || strlen($filteredText) > 500 || !$artId) {

            Utilities::setHeader(400, "comment");
            return false;
        }

        $db->catchMistakes("INSERT INTO COMMENTS (ART_ID, AUTHORID, CONTENT, CREATED) VALUES(?, ?, ?, CURRENT_TIMESTAMP)",
        [$artId, $token->id, $filteredText]);

        return (int) $db->catchMistakes("SELECT MAX(ID) FROM COMMENTS")->fetchColumn();
    }


    /**
      * Deletes current comment (defined in $this->defineInfo) by
      *  replacing content with 'deleted' and authorid with Deleted User's id
      */
    public function delete() {

        $User = new User();
        $db = new MyDB();
        $token = $User->getJWT();

        if (!$User->isLoggedIn() || ($token->id != $this->authorId && $token->level < 3)) {

            Utilities::setHeader(401);
            return false;
        }

        $db->catchMistakes("UPDATE COMMENTS SET AUTHORID = (SELECT ID FROM USERS WHERE USERNAME = ?), CONTENT = ? WHERE ID = ?",
        ["Deleted", "deleted", $this->id]);

        $this->defineInfo($this->id); // load new info into class

        return true;
    }

    public function getContent() {

        return $this->content;
    }


    public function getCreated() {

        return $this->created;
    }

    public function getAuthorId() {

        return $this->authorId;
    }

    public function getArticleId() {

        return $this->artId;
    }

    public function getAllByUserId(int $authorId) {

        $db = new MyDB();

        $query = $db->catchMistakes("SELECT ID, ART_ID, CONTENT, CREATED FROM COMMENTS WHERE AUTHORID = ?
          ORDER BY CREATED ASC", $authorId);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        return $query->fetchAll();

    }

    public function getAllByArticle(int $issue, string $url) {

        $db = new MyDB();
        $User = new User();

        // if private and user not logged in
        if (!$db->catchMistakes("SELECT ISPUBLIC FROM ISSUES WHERE NUM = ?", $issue) && !$User->isLoggedIn) {
            return false;
        }

        $query = $db->catchMistakes("SELECT COMMENTS.ID, AUTHORID,
                                     CONCAT(F_NAME, ' ', IFNULL(M_NAME, ''), ' ', L_NAME) AS AUTHOR_NAME,
                                     USERNAME, CONTENT, CREATED
                                    FROM COMMENTS
                                    LEFT JOIN USERS
                                    ON USERS.ID = AUTHORID
                                    WHERE ART_ID = (
                                        SELECT ID FROM PAGEINFO WHERE ISSUE = ? AND URL = ?
                                    )
                                    ORDER BY CREATED ASC", [$issue, $url]);
        $query->setFetchMode(PDO::FETCH_ASSOC);

        return $query->fetchAll();
    }


    /**
      * Strips unwanted tags from comment
      *
      */
    private function stripTags(string $toStrip) {

        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        $toStrip = $purifier->purify($toStrip);

        return strip_tags($toStrip, "<code><pre><p><a><strong><b><em><i><u><sub><sup><strike><ul><ol><li><q><blockquote><br><abbr>");
    }
}

?>