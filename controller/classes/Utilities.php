<?php


class Utilities {

    /**
      * @param $amount - amount of question marks needed
      *
      * @return string containing question marks separated by space comma
      */
    public function generateQuestionMarks(int $amount) {

        $result = "(";

        for ($i = 0; $i < $amount; $i++) {
            $result .= "?, ";
        }

        // get rid of space and comma for last question mark
        return substr($result, 0, strlen($result) - 2) . ")";
    }


    /**
    *   @param $arr1 - 1 dimensional array
    *   @param $arr2 - 1 dimensional array of equal size to $arr1
    *   @param $sortByNumber - Optional, can choose to sort the arrays by string or number.
    *     Default is by number.
    *   @return A multidimensional array consisting of [$i][0] = all values in $arr2 corresponding to
    *     the unique value (which there can be multiple of in param but will be condensed into one in return)
    *     in $arr1. [$i][1] = the value that corresponds to elements in [$i][0]
    *
    * <h3> Example </h3>
    * <p>
    * $arr1 = [2, 3, 2], $arr2 = ["user1", "user2", "user3"] returns --->
    * [0] =>
    *    [0] =>
    *        [0] => "user1", [1] => "user3"
    *    [1] => 2
    * [1] =>
    *    [0] =>
    *       [0] => "user2"
    *    [1] => 3
    * </p>
    */
    public function splitArrayByUniq(array $arr1, array $arr2, bool $sortByNumber = true) {

        if (count($arr1) != count($arr2)) { // needed for multisort
            return false;
        }

        $sortBy = ($sortByNumber) ? SORT_NUMERIC : SORT_STRING;

        array_multisort($arr1, $arr2, $sortBy);


        $uniqArr1 = array_unique($arr1, $sortBy);


        $uniqKeys = array_keys($uniqArr1);

        $uniqKeys[] = count($arr2);


        $i = 0;

        $return = [];

        while ($i < count($uniqKeys) - 1) {

            $return[] = [array_slice($arr2, $uniqKeys[$i], $uniqKeys[$i + 1]), $uniqArr1[$uniqKeys[$i]]];

            $i++;
        }

        return $return;
    }


    /**
      * @param $index - index of $_COOKIE to extract
      *
      * @return the value of index, JSON decoded
      */
    public function cookieToArray(string $index) {

        return json_decode(stripslashes($_COOKIE[$index]), true);
    }


    public static function setHeader(int $code, string $problem = "") {

        $headerContent = $_SERVER["SERVER_PROTOCOL"] . " {$code} ";

        $capitalProblem = ($problem == "url") ? strtoupper($problem) : ucwords($problem);

        if ($code == 400) {

            $headerContent .= "Invalid {$capitalProblem}";
       }

        else if ($code == 409) {

            $headerContent .= "{$capitalProblem} Already In Use";
        }

        else if ($code == 401) {
             $headerContent .= "Unauthorized";
        }

        else if ($code == 404) {
             $headerContent .= "Not Found";
        }
        else {
            $headerContent .= $capitalProblem;
        }

        header($headerContent);
    }


}


?>