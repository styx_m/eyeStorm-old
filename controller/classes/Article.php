<?php

require_once(__DIR__."/../vendor/autoload.php");


class Article {

    protected $url, $issue, $lede, $pics, $body, $created, $authorId, $tags, $views, $id, $isPublic, $displayOrder, $slideImg, $settingsChanged;

    /**
      * Saves settings to db if any have been changed in current instantiation of this class
      *
      * @return true if changes have been made and saved, else (if no changes have been made that must be saved) returns false
      */
    public function __destruct() {

        $User = new User();

        $token = $User->getJWT();

        if (!$this->settingsChanged || $this->settingsChanged && (!$User->isLoggedIn() || ($token->id != $this->authorId && $token->level < 3))) {

            return false;
        }

        $db = new MyDB();

        $db->catchMistakes("UPDATE PAGEINFO SET CREATED = ?, URL = ?, LEDE = ?, IMG_URL = ?, BODY = ?, ISSUE = ?, VIEWS = ?, DISPLAY_ORDER = ?, SLIDE_IMG = ? WHERE ID = ?",
                           [$this->created, $this->url, $this->lede, json_encode($this->pics), $this->body, $this->issue, $this->views, $this->displayOrder, json_encode($this->slideImg), $this->id]);

        $this->tags = array_merge($this->tags, [null, null]); // in case of less than 3 tags

        $db->catchMistakes("UPDATE TAGS SET TAG1 = ?, TAG2 = ?, TAG3 = ? WHERE ART_ID = ?", [$this->tags[0], $this->tags[1], $this->tags[2], $this->id]);
        return true;
    }

    /**
      * Deletes current article from database
      */
    public function destroy() {
        $db = new MyDB();
        $User = new User();
        $token = $User->getJWT();

        if (!$User->isLoggedIn() || ($token->level < 3 && $token->id != $this->authorId)) {
            return false;
        }

        $db->catchMistakes("DELETE FROM COMMENTS WHERE ART_ID = ?", $this->id);
        $db->catchMistakes("DELETE FROM TAGS WHERE ART_ID = ?", $this->id);
        $db->catchMistakes("DELETE FROM PAGEINFO WHERE ID = ?", $this->id);

        $this->settingsChanged = false;

        return true;
    }

    /**
      * Creates new article: Separates first paragraph to be lede, replaces img urls with attr data-src
      *   and saves to database
      *
      * @param $url - string, at most 75 chars
      * @param $body - actual article. Must have at least 1 paragraph and a heading {@code<h1>}
      * @param $tags - see $this->validateTags
      */
    public function create(string $url, string $body, array $tags) {

        $User = new User();
        $token = $User->getJWT();

        if (!$User->isLoggedIn() || !$filteredBody = $this->validatePiece($body)) {

            Utilities::setHeader(400, "article");
            exit;
        }

        $this->authorId = $token->id;


        // gets rid of random spaces, p tags, and style=""
        $filteredBody = preg_replace("/(&nbsp;\s)|(&nbsp;)|(<p><\/p>)|(style=\"\")/", "", $filteredBody);
        $filteredBody = trim($filteredBody);



        if (!$this->separatePics($filteredBody)) {

            Utilities::setHeader(400, "image");
            exit;
        }
        if (!$this->setTags($tags)) {

            Utilities::setHeader(400, "tag");
            exit;
        }
        if (!$this->setURL($url)) {

            Utilities::setHeader(400, "url");
            exit;
        }
        $size = count($this->tags);
        if (!$this->separateLede($this->body) || 3 - $size < 0) {

            Utilities::setHeader(400, "article");
            exit;
        }

        $db = new MyDB();

        $Issue = new Issue();
        $maxPub = $Issue->getMax(true);
        $maxPriv = $Issue->getMax();

        if ($maxPub > $maxPriv || (is_null($maxPub) && is_null($maxPriv))) {
            $Issue->create();
        }

        $this->issue = $maxPub + 1;

        $this->tags = array_merge($this->tags, array_fill($size, 3 - $size, null));

        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [$this->issue, $this->url, $this->lede, $this->body, json_encode($this->pics),
                           $token->id, json_encode($this->slideImg)]);

       $db->catchMistakes("INSERT INTO TAGS (ART_ID, TAG1, TAG2, TAG3) VALUES((SELECT ID FROM PAGEINFO WHERE URL = ? AND ISSUE = ?), ?, ?, ?)", [$this->url, $this->issue, $this->tags[0], $this->tags[1], $this->tags[2]]);

        $SendMail = new SendMail();
        $SendMail->articlePublished($this->listTags(), $this->issue, $this->url);    // WHEN GET AROUND, make the tags into a nice list

        Utilities::setHeader(201, "article created");

        $url = "/issue/{$this->issue}/story/$this->url";

        return ["url" => $url];
    }



    /**
      * Can edit current article
      *
      * @param $body - article (string)
      */
    public function edit(string $body) {

        $User = new User();
        $token = $User->getJWT();

        if (!$filteredBody = $this->validatePiece($body)) {

            Utilities::setHeader(400, "article");
            return false;
        }

        $this->authorId = $token->id;


        // gets rid of random spaces, p tags, and style=""
        $filteredBody = preg_replace("/(<p><\/p>)|(style=\"\")/", "", $filteredBody);
        $filteredBody = trim($filteredBody);

        if (!$this->separatePics($filteredBody) || !$this->separateLede($this->body)) {

            Utilities::setHeader(400, "image");
            return false;
        }

        $this->settingsChanged = true;

        Utilities::setHeader(200, "edited");
    }

    /**
      * Acts according to HTTP nouns GET, POST, PUT, DELETE for articles
      *
      * GET - get article
      * POST - create
      * PUT - edit
      * DELETE - delete
      */
    public function route(string $method, array $data) {

        $User = new User();
        $token = $User->getJWT();

        if ($method != "POST") {

            // the if statement is for when running test for article with space in name.
            // Couldn't get cURL to transfer it like it is being transferred by an actual browser request
            $this->defineInfoFor($data["issue"], ($token->automatedTest) ? rawurldecode($data["name"]) : $data["name"]);
        }

        if ($method == "DELETE") {

            $User->defineInfoFor($token->id, true);

            if (!$User->checkPassword($data["password"])) {
                return false;
            }
        }

        switch($method) {

            case "GET":

                $Comment = new Comment();

                if ((!$User->isLoggedIn() && !$this->isPublic) || !strlen($this->lede)) {

                    Utilities::setHeader(404);
                    exit;
                }

                return ["BODY" => $this->getBody(), "TAGS" => $this->listTags(),
                       "COMMENTS"=>$Comment->getAllByArticle($this->issue, $this->url), "ID"=> $this->id,
                       "CAN_EDIT" => $this->canEdit()];

            case "POST":
                    return $this->create($data["name"], $data["txtArea"], $data["type[]"]);

            case "PUT":

                if (($token->id == $this->authorId && !$this->isPublic) || $token->level > 2) {
                    return $this->edit($data["edit"]);
                }

            case "DELETE":
                return $this->destroy();
        }
    }

    /**
      * Given an article, this separates the article up until first {@code</p>} into $this->lede
      *  and the rest of the article into $this->body
      *
      * @param $filteredBody - article (string)
      *
      * @return true
      */
    private function separateLede(string $filteredBody) {

        preg_match("/[^\/>]<\/p>/", $filteredBody, $firstP);

        if (!$firstP) {
           preg_match("/<\/p>/", $filteredBody, $firstP);
        }

        $this->lede = substr($filteredBody, 0, strpos($filteredBody, $firstP[0]) + 5);

        $this->body = substr($filteredBody, strpos($filteredBody, $firstP[0]) + 5);

        return true;
    }

    /**
      * Given an article, this separates all pictures into $this->pics
      *   and replaces all `src="*"` with `data-src` from $filteredBody, then saves it to $this->body
      *
      * @param $filteredBody - string
      *
      * @return false if there's an invalid pictures (according to $this->validatePics), else returns true
      */
    private function separatePics(string $filteredBody) {

       preg_match_all('/src="([^"]+)"/', $filteredBody, $pics);

       $this->pics = $this->validatePics($pics[1]);

       $this->slideImg = array_fill(0, count($this->pics), 1);


       if (!$this->pics && !empty($pics[1]))  {
           return false;
       }

       $this->body = preg_replace('/src="[^"]+"/', "data-src", $filteredBody);

       preg_match_all("/<img.[^>]+/", $this->body, $images);

       for ($i = 0; $i < count($images[0]); $i++) {

           if (strpos($images[0][$i], "previewHidden")) {
               $this->slideImg[$i] = 0;
           }
       }

       return true;
    }

    /**
      * Brings up all information about article specified in by parameters from the database,
      *   and initialized all instance variables that holds information about it (other than $this->changeSettings)
      *
      * @param $issue - issue the article is in
      * @param $name - name of issue
      *
      * @return false if nothing in db matches parameters given, else returns id of article
      */
    public function defineInfoFor(int $issue, string $name) {

        $db = new MyDB();

        $filteredIssue = filter_var($issue, FILTER_SANITIZE_NUMBER_INT);
        $filteredName = filter_var($name, FILTER_SANITIZE_STRING);

        $allInfo =
        $db->catchMistakes("SELECT PAGEINFO.URL, PAGEINFO.ISSUE, PAGEINFO.LEDE, PAGEINFO.BODY, PAGEINFO.IMG_URL, PAGEINFO.CREATED,
                                PAGEINFO.AUTHORID, TAG1, TAG2, TAG3, PAGEINFO.VIEWS, PAGEINFO.ID, PAGEINFO.DISPLAY_ORDER, PAGEINFO.SLIDE_IMG, ISSUES.ISPUBLIC
                            FROM PAGEINFO
                            LEFT JOIN ISSUES
                            ON PAGEINFO.ISSUE = ISSUES.NUM
                            LEFT JOIN TAGS ON TAGS.ART_ID = PAGEINFO.ID
                            WHERE PAGEINFO.ISSUE = ? AND PAGEINFO.URL = ?", [$filteredIssue, $filteredName]);

        $allInfo->setFetchMode(PDO::FETCH_ASSOC);

        $dbVals = $allInfo->fetch();

        $allInfo->closeCursor();


        if (!$dbVals) {
            return false;
        }
        //echo $dbVals["IMG_URL"];exit;
        $this->isPublic = $dbVals["ISPUBLIC"];
        $this->displayOrder = $dbVals["DISPLAY_ORDER"];
        $this->slideImg = json_decode($dbVals["SLIDE_IMG"]);
        $this->url = $dbVals["URL"];
        $this->issue = $dbVals["ISSUE"];
        $this->lede = $dbVals["LEDE"];
        $this->body = $dbVals["BODY"];
        $this->pics = json_decode($dbVals["IMG_URL"]);
        $this->created = $dbVals["CREATED"];
        $this->authorId = $dbVals["AUTHORID"];
        $this->tags = array_filter([$dbVals["TAG1"], $dbVals["TAG2"], $dbVals["TAG3"]]);
        $this->views = $dbVals["VIEWS"];
        $this->id = $dbVals["ID"];

        return $dbVals["ID"];
    }

    public function setDisplayOrder(int $num) {
        $this->getDisplayOrder = ($num > -1 && !is_nan($num)) ? $num : 0;
    }

    public function getDisplayOrder() {
        return $this->displayOrder;
    }

    /**
      * Sets url of article
      *
      * @param $url - new url, according to $this->validateURL
      *
      * @return true if valid url, else false
      */
    public function setURL(string $url) {

        if (!$filtURL = $this->validateURL($url)) {
            return false;
        }
        $this->url = $filtURL;
        $this->settingsChanged = true;
        return true;
    }

    /**
      * @param $url - string
      *
      * @return false if not valid according to built in FILTER_VALIDATE_URL, or $url > 75 chars
      *   else returns sanitized version of url
      */
    private function validateURL(string $url) {

        if (!filter_var(FILTER_VALIDATE_URL) || strlen($url) > 75 || !is_string($url) || strlen($url) < 2) {

            Utilities::setHeader(400, "url");
            return false;
        }

        $url = preg_replace("/[><]/", "", $url);

        return rawurlencode(filter_var($url, FILTER_SANITIZE_STRING)); // not doing sanitize url since that removes spaces
    }

    /**
      * Getter for $this->url
      */
    public function getURL() {
        return $this->url;
    }

    /**
      * Sets issue of article
      *
      * @param $issue - number
      *
      * @return false if $issue invalid according to $this->validateIssue, else returns true
      */
    public function setIssue(int $issue) {

        if (!$filtIssue = $this->validateIssue($issue)) {
            return false;
        }

        $this->issue = $filtIssue;
        $this->settingsChanged = true;
        return true;
    }

    /**
      * @param $issue - number
      *
      * @return false if $issue isn't a number, or if it's less than 0, else returns sanitized version of $issue
      */
    private function validateIssue(int $issue) {
        // TODO: check if $issue is greater than max issue
        if (!preg_match("/^\d+$/") || $issue < 1) {

            Utilities::setHeader(400, "issue");
            return false;
        }

        return $filter_var($issue, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
      * Getter for $this->issue
      */
    public function getIssue() {
        return $this->issue;
    }

    /**
      * Saves $lede as lede of article
      *
      * @param $lede - string
      *
      * @return false if $lede invalid according to $this->validatePiece, else return true
      */
    public function setLede(string $lede) {

        if (!$filtLede = $this->validatePiece($lede)) {
            return false;
        }

        $this->lede = $filtLede;
        $this->settingsChanged = true;
        return true;
    }

    /**
      * Getter for $this->lede
      */
    public function getLede() {
        return $this->lede;
    }

    /**
      * Sets $body as body of article
      *
      * @param $body - string, valid acc to $this->validateBody
      *
      * @return false if invalid, else true
      */
    public function setBody(string $body) {

        if (!$filtBody = $this->validatePiece($body)) {
            return false;
        }

        $this->body = $filtBody;
        $this->settingsChanged = true;
        return true;
    }

    /**
    * @return article without things needed for formatting (used for certain tests)
    */
    public function getBody() {

        $User = new User();
        $Utilities = new Utilities();

        if (!$User->isLoggedIn() && !$this->getPublic()) {

            Utilities::setHeader(404);
            return false; // not exit since phpunit stops if it is
        }

        $content = $this->lede . $this->body;

        foreach ($this->pics as $pic) {

            $pos = strpos($content, "data-src");

            if ($pos !== false) {
                $content = substr_replace($content, "src='{$pic}'", $pos, strlen("data-src"));
            }
        }

        if (!$User->isLoggedIn() && $this->getPublic() && isset($_COOKIE["pagesViewed"]) && !in_array($this->issue. $this->url, $Utilities->cookieToArray("pagesViewed"))) {
            $this->addCookieView();
        }

        if (!isset($_COOKIE["pagesViewed"]) && $this->getPublic() && !$User->isLoggedIn()) {
            $db = new MyDB();
            $db->catchMistakes("UPDATE PAGEINFO SET VIEWS = VIEWS + 1 WHERE ID = ?", $this->id);
        }


        return $content;
    }

    /**
      * @param $piece - string
      *
      * @return false if $piece is empty, does not have a <p> tag, or whose `VIEWS` > 0
      *   If all's well, returns $piece filtered through $this->stripTags
      */
    private function validatePiece(string $piece) {

        if (!strpos($piece, "<p>") || !preg_match("/^<h1>([\s\S]+)<\/h1>[\s\S]*<h4>.+/", $piece)) {

            Utilities::setHeader(400, "article");
            return false;
        }

        return $this->stripTags($piece);
    }

    /**
      * @param $toStrip - string
      *
      * @return $toStrip, stripped of all tags except ones listed in function
      */
    public function stripTags(string $toStrip) {

        $config = HTMLPurifier_Config::createDefault();
        $config->set('URI.AllowedSchemes', ['http' => true,
                                            'https' => true,
                                            'mailto' => true,
                                            'tel' => true,
                                            'data'=>true]);
        $config->set("Attr.DefaultImageAlt", "");
        $purifier = new HTMLPurifier($config);
        $toStrip = $purifier->purify($toStrip);

        return strip_tags($toStrip, "<h1><h2><h3><h4><h5><h6><pre><img><p><a><table><td><tr><th><tbody><thead><tfoot><strong><b><em><i><u><sub><sup><font><strike><ul><ol><li><q><blockquote><br><abbr><div><span>");
    }

    /**
      * Validates img urls
      *
      * @param $pics - array of strings
      *
      * @return false if invalid url, if certain extensions. Else returns $pics
      */
    private function validatePics(array $pics) {

        $acceptedImgExt = ["jpg", "jpeg", "png", "jif", "jfif", "tiff", "tif", "gif", "bmp"];

        $i = 0;

        // can't do data pics when article is created since don't have article id yet
        foreach ($pics as $key => $pic) {

            preg_match("/^.+\.(\w{3,4})/", $pic, $foundPic); // img type

            if (preg_match("/data:image/", $pic)) {
                // data uris are stored in actual files since can't fit in db

                $url = "/../../public/images/issue/{$this->issue}/{$this->id}/";

                if (!file_exists(__DIR__.$url)) {
                    mkdir(__DIR__.$url, 0755, true); //TODO: check if permissions are good
                }

                $imgData = str_replace(' ','+',$pic);
                $imgData =  substr($imgData,strpos($imgData,",")+1);
                $imgData = base64_decode($imgData);
                file_put_contents(__DIR__.$url . "{$i}.png", $imgData);
                $pics[$key] = "/images/issue/{$this->issue}/{$this->id}/{$i}.png";
                $i++;
            }


            $imgFormat = (preg_match("/^\//", $pics[$key])) ? "data" : $foundPic[1]; // data uri or local imags

            if (!filter_var($pics[$key], FILTER_VALIDATE_URL) ||
               (!in_array($imgFormat, $acceptedImgExt) && (strpos($pic, "googleusercontent") == false))) {

                if (!preg_match("/^\//", $pics[$key])) {

                    Utilities::setHeader(400, "image");
                    return false;
                }
            }
       }

       return (empty($pics)) ? [] : $pics;
    }

    /**
      * Getter for $this->pics
      */
    public function getPics() {
        return $this->pics;
    }

    /**
      * Sets article's tags
      *
      * @param $tags - array of string(s)
      *
      * @return false if invalid acc to $this->validateTags, else true
      */
    public function setTags(array $tags) {

        if (!$filtTags = $this->validateTags($tags)) {
             return false;
        }

        $this->tags = $filtTags;
        $this->settingsChanged = true;
        return true;
    }

    /**
      * Validates tags
      *
      * @param $tags - something
      *
      * @return false if not an array, empty array, array is not composed of strings.
      *   Else returns sanitized version of $tags
      */
    private function validateTags(array $tags) {

        if (!is_array($tags)) {
            $tags = [$tags];
        }

        if (count($tags) < 1) {
            return false;
        }

        foreach ($tags as $key => $tag) {

            if (!is_string($tag)) {

                Utilities::setHeader(400, "tag");
                return false;
            }

            $tags[$key] = filter_var($tag, FILTER_SANITIZE_STRING);
        }

        return array_unique($tags);
    }

    /**
      * Getter for $this->tags
      */
    public function getTags() {
        return $this->tags;
    }

    /**
      * Gets number of views article has
      */
    public function getViews() {
        return $this->views;
    }

    /**
      * @return true if article is not public, and user is either lvl 3 or the author of article. Else return false
      */
    public function canEdit() {

        $User = new User();
        $token = $User->getJWT();

        return $User->isLoggedIn() && ((!$this->isPublic && $token->id == $this->authorId) || $token->level > 2);
    }

    public function exists(int $issue, string $url) {

        $db = new MyDB();
        $User = new User();

        return !!$db->catchMistakes("SELECT ID FROM PAGEINFO
        LEFT JOIN ISSUES
        ON NUM = ISSUE
        WHERE ISSUE = ? AND URL = ? AND (ISPUBLIC = ? OR ?)", [$issue, $url, !$User->isLoggedIn(), (int)$User->isLoggedIn()])->fetchColumn();
    }

    /**
      * If cookie 'pagesViewed' is set, adds current article to it and increases article's view by 1
      */
    private function addCookieView() {

        // if article is public, add cookie that shows you visited page
        if (isset($_COOKIE["pagesViewed"]) && $this->getPublic()) {

            $cookie = $_COOKIE['pagesViewed'];
            $cookie = stripslashes($cookie);
            $savedCardArray = json_decode($cookie, true);

            if (!in_array($this->issue . $this->url, $savedCardArray)) {

                $savedCardArray[] = $this->issue . $this->url;

                $json = json_encode($savedCardArray);

                setcookie('pagesViewed', $json, null, "/");

                $db = new MyDB();
                $db->catchMistakes("UPDATE PAGEINFO SET VIEWS = VIEWS + 1 WHERE ID = ?", $this->id);
            }
        }
    }

    /**
      * @return true if article is public, else false
      */
    public function getPublic() {
        return $this->isPublic;
    }

    /**
      * @return author's id
      */
    public function getAuthorId() {
        return $this->authorId;
    }

    /**
      * @return article's id
      */
    public function getId() {
        return $this->id;
    }

    /**
    * @param $encodedTags is a json encoded array of tags
    * @return comma separated list of tags (string)
    */
    public function listTags() {

        $toReturn = "";

        foreach ($this->tags as $elt) {
            $toReturn .= $elt . ", ";
        }

        return substr($toReturn, 0, strlen($toReturn) - 2);
    }

    /**
      * @return date article was published
      */
    public function getCreated() {
        return $this->created;
    }

}



?>