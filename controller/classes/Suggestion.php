<?php

class Suggestion {

    private $currentUrl;

    /**
      * @param $currentUrl - url of page
      */
    public function __construct(string $currentUrl) {

        $this->currentURL = substr($currentUrl, strrpos($currentUrl, "/") + 1);
    }

    /**
      * @return table containing possible things url could have been trying to go to
      */
    public function getSuggestions() {

        return $this->formatResults([$this->storySuggestions(), $this->userSuggestions()]);
    }

    /**
      * @return public articles that partially match $this->currentUrl
      */
    private function storySuggestions() {

        $db = new MyDB();

        $query = $db->catchMistakes("SELECT ISSUE, URL FROM PAGEINFO WHERE URL LIKE ? AND (
            SELECT ISPUBLIC FROM ISSUES WHERE URL LIKE ? LIMIT 1
            ) = 1
            ORDER BY ISSUE DESC",
            ["%{$this->currentURL}%", "%{$this->currentURL}%"]);

        return $query->fetchAll(PDO::FETCH_ASSOC);

    }

    /**
      * @return usernames of users whose first, last, or username partially match $currentUrl
      */
    private function userSuggestions() {

        $db = new MyDB();

        $query = $db->catchMistakes("SELECT CONCAT(F_NAME, ' ', IFNULL(M_NAME, ''), ' ', L_NAME) AS NAME,
          TRIM(TRAILING '@tabc.org' FROM EMAIL) AS PROFILE_LINK
          FROM USERS WHERE F_NAME LIKE ? OR L_NAME LIKE ? OR EMAIL LIKE ?",
          ["%{$this->currentURL}%", "%{$this->currentURL}%", "%{$this->currentURL}%"]);

        return $query->fetchAll(PDO::FETCH_ASSOC);

    }

    /**
      * @param $suggestions - 2d array containing results of $this->storySuggestions and
      *   $this->userSuggestions in that order,
      *
      * @return 2 col table where each col has information from its respective index of array
      *   If one array is longer than the other, the table puts blank cells in where needed
      */
    private function formatResults(array $suggestions = []) {


        $html = "<div class='tableContainer container'>
                  <table>
                     <caption><h4>Perhaps you meant...</h4></caption>
                     <thead>
                         <th>Articles</th>
                         <th>Users</th>
                     </thead>
                     <tbody>";

        for ($i = 0; $i < max(count($suggestions[0]), count($suggestions[1])); $i++) {

            $html .= "<tr>
                       <td>";

                     if (count($suggestions[0]) > $i) {

                $html .= "<a href='/issue/{$suggestions[0][$i]["ISSUE"]}/story/{$suggestions[0][$i]["URL"]}'>
                             ".urldecode($suggestions[0][$i]["URL"]) .
                          "<a>";
                      }

            $html .=   "</td>
                        <td>";

                     if (count($suggestions[1]) > $i) {

                 $html .= "<a href='/u/{$suggestions[1][$i]["PROFILE_LINK"]}'>
                              {$suggestions[1][$i]["NAME"]}
                           </a>";
                     }
            $html .=   "</td>
                      </tr>";
        }

        $html .= "</tbody>
                </table>
              </div>";

        return $html;
    }





}