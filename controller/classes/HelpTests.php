<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once("../db.php");
require_once("../classes/User.php");


$User = new User();
$User->changeJWT(["automatedTest" => true]);

// generic stuff that's needed in more than 1 test file
class HelpTests {

    public static $url = ["testUrl", "testArt", "testAnother", "testOneMore"]; // first 2 are for public articles, last two for private

    public static $adminId = 25; // random number
    public static $test1Id = 26;

    public static $adminEmail = "randomName2018@tabc.org";
    public static $test1Email = "lessRandomName2018@tabc.org";

    public static $pubIssue, $privIssue, $pubId, $privId;

    public static $pics = ["https://static.wixstatic.com/media/1af70b5973771d67736218fc1f7b24c8.jpg/v1/fill/w_495,h_358,al_c,q_80,usm_0.66_1.00_0.01/1af70b5973771d67736218fc1f7b24c8.jpg"];

    public static $adminPass = "a97A65";
    public static $lede = "<h1>Opinion: Why Does College Matter So Much for Professional Advancement and Financial Security?</h1>\n<h4>January 11, 2017|Ezra Friedman, Ezra Finkelstein, and Aharon Nissel</h4>
        <img alt='' src=''>
        <p> This is a paragraph of extremely interesting text</p>";

    public static $body = "<p>This is another paragraph of the most interesting thing you've read all year</p>";

    public static $tags = ["reaction", "opinion", null];

    public static $unsafeStrings = ["<script>hi</script>", "<p onclick='alert(hi)'>hi</p>", "<?php echo 'hi'?>"];

    public static $basicArticle = "<h1>Title</h1><h4>Author</h4><p>Lede</p><p>Body</p>";

    // a 1x2 px screenshot of black screen
    public static $dataURI = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAYLmlDQ1BJQ0MgUHJvZmlsZQAAWIWVWQVYVcu3n71PAofu7u7ubgHpFJTDoeMIhxTBAC8imIiIhCAgpaRKCSgYiKJIqKCIIKIgqCCKSr5N6L3/+9733vfm
        +/beP2bWrPmtWWticQDgZiGGh4fCDACEkaMoDuZGAm7uHgK4cYAC3IANiAFZIiky3NDOzhog5ff3P8vicwBtfAdkN3T99/b/tTD6+kWSAIDsEOzjG0kKQ/B1ANAcpHBKFACYHqReODYqfAPPIZiFghAEAIvewAFbmGMD+2xhmU0ZJwdjBBsAgCcQiZQAAOg2eAvEkAIQPXQIRywT2TeIjIgeQrAeKZDoCwDXbURGJixs7waeRbCEzz/0BPyHTp8/OonEgD94y5bNgjcJigwPJe77f07H/13CQqN/jyGEPIRAioXDhs3IvJWH7LXawAQEt5J9dtoimAnBD4J8N+U38MvAaAvnbflZUqQxMmeInwEMfIkmVgjmQTBbdIiz4TZWIlI2+yLy8M6gKEunbexD2euwrR+OIYfutN7WkxroZ/kbX/SLNHX8LeMfZGaJYCTS4OvxgU6uWzzhuzFBLjsRTIfgp5EhjlbbfUfjA413/pahRDtscBZB8Dd/ipnDlgyKIyzyt10oORJxcywkFlAGUYFOFlt9UW5+kW7Wvzn4+pmYbnFA+fqRnbe5oZDoMnLY7psSHmq3LY+66Bdq7rA1z6jayBjH3337o5AA25oH1Ntg4g677bEWw6PsnLa4oWFgDYyBCRAA0cjjA/aCYBD0ZLZxFvlrq8UMEAEFBAA/ILtd87uH62YLGXk7gnjwCUF+IPJPP6PNVj8Qg9Sv/qndessC/83WmM0eIeA9gsPQXGg9tDbaGnkbII8SWgOt+bufAP3vUbGmWBOsBdYMK/mHBwlhHYo8FBD0P9RZIV8/xLoNLuTfNvytD/Me04d5i3mGGcMMAxfwblPLttSeoCTKv5gLABswhmgz27bO55/WocUQ1qpoI7Quwh/hjmZDcwFZtApiiSFaH7FNFan9J8PoP9z+nst/j7fB+p/2bNfTSdGpbrPw+eMZ4z9S/9Zi/I858kW+Vv+WRKWirqG6UB2oblQrqhEIoG6hmlA9qLYN/CcS3m1Gwu/RHDa5hSB6gn7LKFxRmFZY+dfYxO3xN+YrMsovLmpjMRjvDd9HCQoIjBIwRHZjPwFLMklORkBJQVEdgI29fWvrWHDY3LMhtt6/63wUAdBYRbaunr/rwlYAqD4CAN+9v+vEKpClcgCAK3mkaErMVt3GdgwwgBrQI6uCE/ABYSCB2KME1IA2MACmYAewBU7AHexGZjwQhCGcY0ECOAxSQDo4Dc6BHFAAikE5qAL1oBG0gg5wHzwCT8Ez8AqJi0kwA+bAIliGIAgH0ULMECfED4lC0pASpAHpQaaQNeQAuUPeUABEhqKhBCgZSocyoBzoElQB1UHNUAfUDfVBw9A4NA19hZZgFEyAWWBeWAyWhzVgQ9gKdoK94AA4Ao6Hj8An4Wy4CL4KN8Ad8CP4GTwGz8DfUQBFg2JDCaJkURooY5QtygPlj6KgDqDSUFmoIlQ1qgXx8wBqDDWL+oXGopnRAmhZJDYt0M5oEjoCfQB9HJ2DLkc3oO+iB9Dj6Dn0GoYWw4ORxmhhLDFumABMLCYFk4UpxdzA3EPWzSRmEYvFsmHFserIunTHBmP3Y49j87E12NvYPuwE9jsOh+PESeN0cbY4Ii4Kl4K7gLuKu4Xrx03ifuJp8Px4JbwZ3gNPxifhs/CV+HZ8P/4DfpmKgUqUSovKlsqXah/VKaoSqhaqXqpJqmVqRmpxal1qJ+pg6sPU2dTV1PeoR6gXaGhohGg0aexpgmgO0WTT1NI8oBmn+UVgIkgRjAmehGjCSUIZ4TZhmLBAS0srRmtA60EbRXuStoL2Du0o7U86Zjo5Oks6X7qDdLl0DXT9dJ/pqehF6Q3pd9PH02fRX6PvpZ9loGIQYzBmIDIcYMhlaGZ4wfCdkZlRkdGWMYzxOGMlYzfjFBOOSYzJlMmX6QhTMdMdpglmFLMwszEziTmZuYT5HvMkC5ZFnMWSJZglnaWK5QnLHCsTqwqrC2scay5rG+sYG4pNjM2SLZTtFFs923O2JXZedkN2P/Zj7NXs/ew/OLg5DDj8ONI4ajiecSxxCnCacoZwnuFs5HzNheaS4rLniuW6yHWPa5abhVubm8Sdxl3P/ZIH5pHiceDZz1PM08PznZeP15w3nPcC7x3eWT42PgO+YL5Mvna+aX5mfj3+IP5M/lv8HwVYBQwFQgWyBe4KzAnyCFoIRgteEnwiuCwkLuQslCRUI/RamFpYQ9hfOFO4U3hOhF/ERiRB5IrIS1EqUQ3RQNHzol2iP8TExVzFjoo1ik2Jc4hbiseLXxEfkaCV0JeIkCiSGJTESmpIhkjmSz6VgqVUpQKlcqV6pWFpNekg6XzpPhmMjKYMWaZI5oUsQdZQNkb2iuy4HJuctVySXKPcZ3kReQ/5M/Jd8msKqgqhCiUKrxSZFHcoJim2KH5VklIiKeUqDSrTKpspH1RuUp5XkVbxU7moMqTKrGqjelS1U3VVTV2NolatNq0uou6tnqf+QoNFw07juMYDTYymkeZBzVbNX1pqWlFa9VpftGW1Q7Qrtad0xHX8dEp0JnSFdIm6l3TH9AT0vPUK9cb0BfWJ+kX6bw2EDXwNSg0+GEoaBhteNfxspGBEMbph9MNYyzjR+LYJysTcJM3kiSmTqbNpjumomZBZgNkVszlzVfP95rctMBZWFmcsXljyWpIsKyzndqjvSNxx14pg5WiVY/XWWsqaYt1iA9vssDlrM7JTdCd5Z6MtsLW0PWv72k7cLsLupj3W3s4+1/69g6JDgkOXI7PjHsdKx0UnI6dTTq+cJZyjnTtd6F08XSpcfriauGa4jrnJuyW6PXLncg9yb/LAebh4lHp832W669yuSU9VzxTP517iXnFe3bu5dofubttDv4e455o3xtvVu9J7hWhLLCJ+97H0yfOZIxmTzpNmfA18M32n/XT9Mvw++Ov6Z/hPBegGnA2YDtQPzAqcDTIOygmaD7YILgj+EWIbUhayHuoaWhOGD/MOayYzkUPId/fy7Y3b2xcuHZ4SPhahFXEuYo5iRSmNhCK9IpuiWJBrTk+0RPRf0eMxejG5MT9jXWKvxTHGkeN69kntO7bvQ7xZ/OX96P2k/Z0JggmHE8YTDRMvHYAO+BzoPCh88MjByUPmh8oPUx8OOfw4SSEpI+lbsmtyyxHeI4eOTPxl/teVFLoUSsqLo9pHC1LRqUGpT44pH7twbC3NN+1hukJ6VvrKcdLxhycUT2SfWD/pf/LJKbVTF09jT5NPPz+jf6Y8gzEjPmPirM3ZhkyBzLTMb+f2nOvOUskqOE99Pvr8WLZ1dtMFkQunL6zkBOY8yzXKrcnjyTuW9yPfN7//osHF6gLegvSCpcKgwqFL5pcaisSKsoqxxTHF70tcSroua1yuKOUqTS9dLSOXjZU7lN+tUK+oqOSpPHUFvhJ9Zfqq59WnVSZVTdWy1Zdq2GrSa0FtdO3HOu+65/VW9Z3XNK5VXxe9nneD+UZaA9Swr2GuMbBxrMm9qa95R3Nni3bLjZtyN8taBVtz21jbTrVTtx9pX78Vf+v77fDbsx0BHROdezpf3XG7M3jX/u6Te1b3Htw3u3+ny7Dr1gPdB63dWt3NDzUeNj5Se9TQo9pz47Hq4xtP1J409Kr3Nj3VfNrSp9PX3q/f3zFgMnB/0HLw0bOdz/qeOz8feuH5YmzId2hqOHR4/mXMy+VXh0YwI2mvGV5njfKMFr2RfFMzpjbWNm4y3vPW8e2rCdLEzLvIdyuTR97Tvs/6wP+hYkppqnXabPrpx10fJ2fCZ5ZnUz4xfsr7LPH5+heDLz1zbnOT85T59a/HFzgXyr6pfOv8bvd9dDFscflH2k/On+W/NH51LbkufViOXcGtZK9KrrasWa2NrIetr4cTKcTNqwAKeWB/fwC+lgFA6w4A81MAqOm2cq/tgoI2Ug4AXCA5aAY+glJCTaELMUTkrJvDNeGPUwVQ69OwEBhomegY6FkYeBgFmaSZtVhsWH3Y4tjPclRz9nBNcX/nWeXD83MIiAoqCOkJ24p4i0aKpYrnS9RLPpbGyBBl2+UFFPYrDipLqcSqdqpjNYw1o7SKtXt0vuhR6zMasBpyGHEac5mwm7KY0ZvjzdctvlnO7HhjNWB936ZxZ7ltjt0J+2SHeMcopwjnSJdY1wNuqe6ZHkW7qjyrvSp3l+4p9r5ELPApJF3yLfGr8K8JaA68E9QbPBoyH4Ylc+2VDteKsKA4R+6O8osOjYmIjYnbty8x/tD+/ISWxIEDHw+uHaZL4k4WOyL3l2qK1lG9VKNj5mn26ZTjeSe6T/48LXnGM+PE2fbM2Sy289rZnhfick7nFuc15D+8+Lpg/hK6iK1YokTzsmWpW1lAeXRFUuXJK7lXL1fVVd+q6a0dq1u4hr3OfUOhwbTRvSms+VBLxs1LrSfaotrdb+ne5u8AHW8779wpvZt+j3Lfq2vHA61uvu6ZhyWPXHuwPTWPnR7/fJLTq987+fRsn2nfSv/NgaRB22f8zz4/b3uRNuQ8zD889bLqVcSI8si31/Wj4W+U3qyOPRkveBs+YfCO8d3bybr3hz/YTfFPfZq++fHYjNMs5+zwp/OfPb4If/k192F+fkHy26lF6Z/0S/Yrs+vrm/4Xhmpha3gOlYU2Q3/HlGF9cCK4KXw1VRS1IY0ogZGwRDtD94r+KcM9xnamRuYmllbWNrYO9m6OPs4urjbuBp463it85fylAhcFzwqdFi4W6RL9LM4mYSAZKpUj/UBmUU5M3lHhgGKlUp/yoiqHmoa6rQZRM1iLpO2uY6NrqKeiL2rAYggbfjF6ZXzfpNY0x+yIeaiFg6XaDm4rYPXB+onNjZ0FtsfsYuxJDg6ORk5KziIubK5412W3T+5vPPp23fVsRKKhYE+mdxrxkE8MiexL9iP7hwWEBoYEBQcHhviFEsN2kz32uoY7RthSbCLdo/yRK3NibErcyX2Z8Rf25yXkJxYcKDxYeKjwcHFSQ/KTI1MpmKNCSDyQ0pLTi493nBg9+eM03RnhDPWz1pk+5+KyTp0vyW650Jszkfsjn+oiV4F0odYl8yKX4sCSA5czS6vKHpS/qwRXeK9qV7lWU2qO116ua61/eu319ekb3xuhJtpmzhbRmwqtmm1G7Ttu2d127vDo3HXH6a7lPZ37sl3cD3AP5rqfP2x+lNuT8NjziXYvV+/Pp8/7rvefHYgadHtm8Fz2Bf8QxzD7S75XUiM6rx1Hfd54jJmPq7wVnKCbWHv3eXL8/YsPvVOPprs/ds88nh349PELNMc+L/PVZMH7W/L3usWZnzq/Cpc5VirXrDf9Lw3uQlbQEBIB91GWqGdob/Q3TApWENuIc8Qt4MuovKiZqZ/SnCQ40ArQztE9pL/KkMl4mCmKOYTFi1WXjYNtAYmEQs4YLituQe4fPI95S/gS+V0FlAUZBD8INQunijiJ8oqOiRWJkyREJd5LVkjtldaQQcv0yebIEeUl5GcV6hRjlLSU1pQ7VI6omqvh1R6qp2vYaFJp3tdK1jbQXtFp0o3SU9D7qH/ZwNuQy3DQ6KSxpfGayTXTYDN+swHzoxbaFp8sC3bYW0FW9dY+Now293Ym2mrZ/rC7bk9B7g9fHKudyM6yzp9cqlzD3GTcptyLPNx3sezq8zzt5bibc/fbPdXe+4lWPjw+n0kdvhl+nv7C/jMB1wITgkyDGYJHQipD48NsyELkX3v7w8siwinKlMXI5qjEaOMYQsxQbFlcwj7XePX93AmohM+JwwfuHKw6dP7w4aSQZJcjhn/JpfAepT26ljp/7H3ai/T245dOJJ/0O2VxWvYM85nVjI9nRzL7zz3OenS+N3vwwlDOSO6bvHf5Uxe/FCxeAkU0xZwlkpd1Su3LAssPVWRX1l7pujpW9auGuVa2zrI+4FrK9dIbDxpmmxib9VtSbg608bX736q6vdCpfeevu333RboSHrx6aPSo7rHUk4Kn6D5if8eg0rOWF5Rh0qvLo57jFZMDM8QF4Q3/b/0PbqNg1QA4x4dkqAwAOJ4D4ARyQIh/AYCdGgA7WgCcNAG8QxfALPEAMuX7c35ASOJJDZgAD5JtqiF5tCsIBodAJqgAt8EQmIcIkASSG5KgJKgYugt9gGlgBdgVToJr4NcoAkoXFY4qQQ2jGZAc7TC6Ff0To4aJxFzBvMGyYq2wSdibSI6liCPjqnFfkFwqDn+bikDlQVVJtYZkSTU0zDT7aMYJ1oRmWinafCTTOY7kNqlINnOSkZWxgEmWqZXZhvkNC4UVy5rDJsfWye7KPsuRxMnJWcflwLXEXcJjx7PKW8XnxU/P3ykQIygtOCaUJbxTBCvSJholJis2LV4hESqpJLks1S2dI0OWNZXjl1uRf6XQpnhcyUtZSYVGZUq1W61GPUfjmGaCFkU7TCdIN0gvWN/fwMZQxYjLGBi/R27IDWaXzE9aHLCM3BFqFWK91yZ+Z5rtJbtm+wGHL060zrIu9q6xboXuPR7LnrJePrtz9jwjMvs4krJ9X/sLBQQEVgUthGiFJof17OUI949ojKSOIkbfjOWNa4n3SsAnNh8MPyydNH2kLIWUynNsIP3oCY1TimfIZx9lWWZP5B67KFlYVyx4eV9ZQ8XbqyzVNrUn64dvyDdmtECtCbfQHVl34fteD1ofCTw+2vut339w9MWu4dcjfqNfx8+/2/1BdnpqNvJz+9zDr+e/KX4vX1z/KffLccl+2XpFbhW9OrR2et18c/+AAAYQACsQBPJAH9gDP5AAMhDvd4IR8BNihVQgJygGyoZaobcwFawI74LT4ZvwJ5QAygV1HHUPDaEN0PvR19GzGDEMEZOPeYnlwLpis7Evcbw4H1wFbh6vjU/FP6cSp4qn6qOWpj5GPUezi6aboEm4SitEm0fHQZdNz0VfyCDBcI3RkHGQKZgZZs5n0WQZYo1j42JrY/fhwHPUcnpwYbiucZN4WHke8ibxafP94L8hEC2oJrgkdEv4qIidKKvoiFipOEXCUJJJ8r1Um/R5mUhZBzlleXb5VYUJxXalPOVEFaKqpZqKurAGqyaNFkYb0gG6sB5On9YAY7BkOGc0afzKpN+026zTvM2ixbJlR5vVPes+m/Gd3+0I9sIO2o7OyC3mtEut66DbkofwLlvPg171u6e8hYh7fPJIL/wY/a0DjgZ2Bq0i3o4LayIvhetHpFIGo0SiY2N641T25e3HJlASxw86HLqfpJvc9Jd6SnOq/rGH6W7HP548elrtzMezpeeCzqteoM6Zyuu92FZYX3S15EppVXlDZcfVx9XPa4frn11/0FDbdKLFu1W6be5WdUfgHfN7u7oiutMeXX58q3e4b34Q85x1SOilzIjKqOaY5lved5jJ+Q9j009mWj4VfImbN12Av1Us6v24/8tgqWKFZpW0dm3T/zDAAxYgjKx9a+APkkEBuAXGIDSy6ndCUVAudAeahdlgQzgSroDHUbyoXah81DhaHB2OvonBY1wx5Zg1rDu2GceHO4b7hQ/Fv0XW9wC1DXU3jQ3NIMGb8IU2iY6T7jq9I/0iQz6jBeMyUx1zKIs0y2fWBrZkdkcOSU6Y8w1XF3c9TzHvBb5z/OeRe8hVxKtDIjOiv8RpJAQk1aUcpSNkzso2y00oMClaKZ1RHlRlU3NWP6PxSAutrasTq3tNb97A2DDX6IeJi2mjOafFfssRKx3rHJtftq52Lxz8HZecz7iKuTV5mO566GW+u91bnphHovZN9PsWEBb4MTgo5GNYKPlzeETEt8jYqKWYxDjOfXf2RyQKHXh26GiSevLEX2lHpVO70vakfz2RdIrjdF2G9dnpc6nnxbO7cvzzMPlFBfqFL4uiSugul5UZlo9WJl7lq7pd41tHVV973eXGamNJs1XL19YL7Qa3PnScusN7N/M+fdeJbvqH53r4Hpf3Kj1t77cY6H/m8nxoyGW495XJyPVRwTd/jc2+9ZgYmtzzfmqKPD014zBb82nui8CczrzxV7UFgYVP31q+Uxa5Fjt/EH+8/xn6c/5X1K+XSwZLpcu0y5HL91foV1xXClemVhVX9612rs6v8a5ZrkWvFa71rP1cF1m3W9+/XrY+sOH/SH9lpc3jAyIYAYAZXV9fEAMAlwHA6pn19eWi9fXVYiTZGAHgdujW7zqbZw1yxuQtbKCHgrn/7feV/wLmt9k4fURB2wAAAZlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iPgogICAgICAgICA8ZXhpZjpQaXhlbFhEaW1lbnNpb24+MTwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj4yPC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+ChQgsi0AAAAQSURBVAgdY2BgYPjPBCQYAAYOAQKrv2M+AAAAAElFTkSuQmCC";


    public static function setupForTests() {

        $User = new User();
        $User->changeJWT(["automatedTest" => true]);

        $_SERVER["SERVER_PROTOCOL"] = "http/1.0";

        HelpTests::createAdmin();
        HelpTests::generateArticles();

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");
    }

    public static function returnToNormal() {

        HelpTests::removeIssues();
        HelpTests::deleteAdmin();
    }

    /**
      * Changes session variables whose values correspond to the parameter names, to the values given
      */
    public static function changeSessions(int $id = null, int $lvl = null, string $email = null) {
        $User = new User();

        $_SERVER["HTTP_USER_AGENT"] = "hi";
        $_SERVER["SERVER_PROTOCOL"] = "http/1.0";

        $User->changeJWT(
            ["id" => $id,
            "level" => $lvl,
            "email" => substr($email, 0, strlen($email) - strlen("@tabc.org")),
            "automatedTest" => true]
            );
    }

    /**
      * Sets $_SESSION to empty array except for automatedTest = true
      */
    public static function endSession() {
        $User = new User();

        $User->logout();
        $User->changeJWT(["automatedTest" => true]);
    }

    /**
      * Creates a lvl 3 user whose username is 'admin' and whose password is 'a97A65'
      */
    public static function createAdmin() {

        $db = new MyDB();


        $db->catchMistakes("INSERT INTO USERS (USERNAME, F_NAME, M_NAME, L_NAME, PASSWORD, LEVEL, EMAIL, NOTIFICATIONS)
          VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
          ["admin", "Head", NULL, "Honcho", password_hash("a97A65", PASSWORD_DEFAULT), 3, HelpTests::$adminEmail, 1]);

        $db->catchMistakes("INSERT INTO USERS (USERNAME, F_NAME, M_NAME, L_NAME, PASSWORD, LEVEL, EMAIL, NOTIFICATIONS)
          VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
          ["test1", "Little", NULL, "Sidekick", password_hash("a97A65", PASSWORD_DEFAULT), 1, HelpTests::$test1Email, 1]);

        $ids = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME IN (?, ?)", ["admin", "test1"]);
        $ids->setFetchMode(PDO::FETCH_COLUMN, 0);

        [HelpTests::$adminId, HelpTests::$test1Id] = $ids->fetchAll();
    }

    /**
      * Deletes user with the username 'admin'
      */
    public static function deleteAdmin() {

        $db = new MyDB();

        $db->catchMistakes("DELETE FROM USERS WHERE USERNAME IN (?, ?)", ["admin", "test1"]);
    }


    public static function generateArticles() {

        $db = new MyDB();

        HelpTests::createIssue(1, HelpTests::$pubIssue);
        HelpTests::createIssue(0, HelpTests::$privIssue);


        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [HelpTests::$pubIssue, HelpTests::$url[0], HelpTests::$lede, HelpTests::$body, json_encode(HelpTests::$pics),
                           HelpTests::$adminId, json_encode([1])]);

        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [HelpTests::$pubIssue, HelpTests::$url[1], HelpTests::$lede, HelpTests::$body, json_encode(HelpTests::$pics),
                           HelpTests::$adminId, json_encode([1])]);

        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [HelpTests::$privIssue, HelpTests::$url[2], HelpTests::$lede, HelpTests::$body, json_encode(HelpTests::$pics),
                           HelpTests::$adminId, json_encode([1])]);

        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [HelpTests::$privIssue, HelpTests::$url[3], HelpTests::$lede, HelpTests::$body, json_encode(HelpTests::$pics),
                           HelpTests::$adminId, json_encode([1])]);



        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [HelpTests::$privIssue, "test1s%20priv", HelpTests::$lede, HelpTests::$body, json_encode(HelpTests::$pics),
                           HelpTests::$test1Id, json_encode([1])]);

        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [HelpTests::$pubIssue, "test1s%20pub", HelpTests::$lede, HelpTests::$body, json_encode(HelpTests::$pics),
                           HelpTests::$test1Id, json_encode([1])]);

                           $artIds = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? OR ISSUE = ?", [HelpTests::$privIssue, HelpTests::$pubIssue]);
                           $ids = $artIds->fetchAll(PDO::FETCH_COLUMN, 0);

       foreach ($ids as $id) {
            $db->catchMistakes("INSERT INTO TAGS (ART_ID, TAG1, TAG2, TAG3) VALUES(?, ?, ?, ?)", [
                                $id, HelpTests::$tags[0], HelpTests::$tags[1], HelpTests::$tags[2]]);

            // the authorid just makes sure that there's 2 for each author
            $db->catchMistakes("INSERT INTO COMMENTS (ART_ID, AUTHORID, CONTENT, CREATED) VALUES(?, ?, ?, CURRENT_TIMESTAMP)",
                                 [$id, ($id % 2 == 0) ? HelpTests::$adminId : HelpTests::$test1Id, "This is valid text"]);
       }

       HelpTests::$pubId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? ORDER BY ID LIMIT 1", HelpTests::$pubIssue)->fetchColumn();
       HelpTests::$privId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? ORDER BY ID LIMIT 1", HelpTests::$privIssue)->fetchColumn();
    }

    // $changeVar not type checked since some stuff pass it in as null
    public static function createIssue(bool $pubStatus, &$changeVar) {

        $db = new MyDB();

        $db->catchMistakes("INSERT INTO ISSUES (NAME, ISPUBLIC, MADEPUB) VALUES(?, ?, ?)", ["testIssue" . rand(), $pubStatus, date("Y-m-d", strtotime("-24 hours"))]); // date is for certain tests

        $changeVar = $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES WHERE ISPUBLIC = ? LIMIT 1", $pubStatus)->fetchColumn();
    }

    public static function removeIssues() {

        $db = new MyDB();

        $db->catchMistakes("DELETE FROM COMMENTS WHERE ART_ID IN (SELECT ID FROM PAGEINFO WHERE ISSUE IN (?, ?))", [HelpTests::$privIssue, HelpTests::$pubIssue]);
        $db->catchMistakes("DELETE FROM TAGS WHERE ART_ID IN (SELECT ID FROM PAGEINFO WHERE ISSUE IN (?, ?))", [HelpTests::$privIssue, HelpTests::$pubIssue]);
        $db->catchMistakes("DELETE FROM PAGEINFO WHERE ISSUE IN (?, ?)", [HelpTests::$privIssue, HelpTests::$pubIssue]);
        $db->catchMistakes("DELETE FROM ISSUES WHERE NUM >= ?", [min(HelpTests::$privIssue, HelpTests::$pubIssue)]);

        $maxIssue = $db->catchMistakes("SELECT IFNULL(MAX(NUM), 1) FROM ISSUES")->fetchColumn();

        $db->catchMistakes("ALTER TABLE ISSUES AUTO_INCREMENT = {$maxIssue}");
    }

    /*************************************************************
    BELOW THIS LINE is functions used to test Router...Test
    *************************************************************/

    /**
      * Helper method to create http request to api while passing along the jwt
      *
      * @param $url - last part of url in a path like http(s)://{domain name}/api/{$url}
      * @param $query - query string (in format of key1=value1&key2=value2 etc)
      * @param $method - http method GET, POST, PUT, or DELETE
      *
      * @return json decoded data given back by the call
      */
    public static function createHTTPRequest(string $url, array $query, string $method) {

        $url = "http://localhost:8888/api/{$url}";
        $ch = curl_init();

        switch ($method) {
            case "GET":
                $url .= "?".http_build_query($query);
                break;

            case "PUT":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
                break;

            case "POST":
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
                break;

            case "DELETE":
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
                break;
        }

         //   curl_setopt($ch, CURLOPT_VERBOSE, true);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // makes response get returned instead of echoing to terminal
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // follow redirects (since .htaccess forces all api stuff through /router)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: jwt={$_COOKIE['jwt']}", "Accept: application/json"));

        $content = trim(curl_exec($ch));
        $res_info = curl_getinfo($ch);

        $api_response_body = substr($content, $res_info['header_size']);

        //echo "\n". debug_backtrace()[2]['function']. " : ".$res_info["http_code"];
        curl_close($ch);
       //echo $content;

        return (empty(json_decode($content))) ? $res_info["http_code"] : json_decode($content);
    }

    /**
      * Creates new user
      *
      * @param $lvl - level user should be
      *
      * @return username of created user
      */
    public static function createNewUser(int $lvl, string $username = "testPop") {

        $db = new MyDB();

        $db->catchMistakes("INSERT INTO USERS (USERNAME, F_NAME, M_NAME, L_NAME, PASSWORD, LEVEL, EMAIL, NOTIFICATIONS)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                            [$username, "Tom" . $username, "M", "Riddle", password_hash(HelpTests::$adminPass, PASSWORD_DEFAULT),
                            $lvl, ".testPop2018@tabc.org", 1]);
                            // adding username to first name to make sure contraint that f,m, and l names otgether must be unique

        $id = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME = ?", $username)->fetchColumn();

        return ["id" => $id, "username" => $username];
    }

    public static function deleteNewUser(string $username = "testPop") {
        $db = new MyDB();
        $db->catchMistakes("DELETE FROM USERS WHERE USERNAME = ?", $username);
    }

    // check if used anywhere else, if not move to RouterUserTest
    // $lvl is not static checked to int since some tests pass it in as null
    public static function createUser($lvl) {

        $db = new MyDB();

        $username = "testAnotherRandom";

        HelpTests::createHTTPRequest("user", ["username" => $username,
                                          "fullName" => "Another A Name",
                                          "password" => "a97A65",
                                          "confirmation" => "a97A65",
                                          "email" => "randomMore2018@tabc.org",
                                          "lvl" => $lvl],
                                          "POST");

        $userExists = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME = ?", $username)->fetchColumn();

        $db->catchMistakes("DELETE FROM USERS WHERE USERNAME = ?", $username);
        return !!$userExists;
    }


        /**
      * Sets sessions
      *
      * @param $user - array where keys are "username" and "id" of user to change to
      * @param $lvl - level to set
      * @param $shouldSet - if should end or create session
      */
    public static function helpSetSessions(array $user, int $lvl, bool $shouldSet = true) {

        if ($shouldSet) {
            HelpTests::changeSessions($user["id"], $lvl, $user["username"]);
        }
        else {
            HelpTests::endSession();
        }
    }






}

/*HelpTests::removeIssues();
HelpTests::deleteAdmin();*/
?>