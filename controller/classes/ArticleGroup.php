<?php

require_once(__DIR__."/../vendor/autoload.php");

class ArticleGroup extends Article {



    public function route(string $method, array $data) {

        if ((!empty($data["tag[]"]) && count($data["artId[]"]) != count($data["tag[]"])) ||
            !empty($data["order[]"]) && count($data["tag[]"]) != count($data["order[]"])) {
            return false;
        }

        switch($method) {

            case "GET":
                $Info = new Info();
                $issue = (is_numeric($data["articlesFor"])) ? $data["articlesFor"] : null;

                return $Info->getPageInfo($issue);

            case "DELETE":

                $result = $this->delete($data["delArt[]"], $data["password"]);

                if ($result) {
                    Utilities::setHeader(200, "article(s) deleted");
                }

            case "PUT":

                for ($i = 0; $i < count($data["artId[]"]); $i++) {

                    $this->setMultiTags($data["artId[]"][$i], $data["tag[]"][$i], $data["password"]);

                    if (!empty($data["order[]"])) {

                        $this->setDisplay($data["artId[]"][$i], $data["order[]"][$i]);
                    }
                }

                if ($i == count("artId[]")) {
                    Utilities::setHeader(200, "article(s) updated");
                }
                return true;
        }
    }

    /**
      * @overide Article parent class's destructor
      */
    public function __destruct() {}



    /**
      * Deletes articles whose id is in array provided
      *
      * @param $ids - array of article ids
      * @param $password - password of user logged in
      *
      * @return false if user is either not lvl 3, not the author of the article provided, or incorrect password
      */
    public function delete(array $ids = [], string $password) {

        $Utilities = new Utilities();
        $db = new MyDB();
        $User = new User();
        $token = $User->getJWT();
        $User->defineInfoFor($token->id, true);


        $inForArts = $Utilities->generateQuestionMarks(count($ids));

        $allAuthors = $db->catchMistakes("SELECT DISTINCT AUTHORID FROM PAGEINFO WHERE ID IN {$inForArts}", $ids);

        $uniqAuthors = $allAuthors->fetchAll(PDO::FETCH_COLUMN, 0);


        // not logged in, less than lvl 3 and not deleting own article, or invalid password
        if (!$User->isLoggedIn() || ([$token->id] != $uniqAuthors && $token->level < 3) ||
           !$User->checkPassword($password)) {

               return false;
        }

        $db->catchMistakes("DELETE FROM COMMENTS WHERE ART_ID IN {$inForArts}", $ids);
        $db->catchMistakes("DELETE FROM TAGS WHERE ART_ID IN {$inForArts}", $ids);
        $db->catchMistakes("DELETE FROM PAGEINFO WHERE ID IN {$inForArts}", $ids);

        return true;
    }

    /**
      * Sets tags of article specified by id
      *
      * @param $artId - id of article
      * @param $newTag - array of tags to give article
      * @param $pass - password of currently logged in user
      *
      * @return false if user's less than lvl 3, not the author of article, or password is incorrect. Else true
      */
    public function setMultiTags(int $artId, array $newTag, string $pass) {

        $User = new User();
        $db = new MyDB();
        $token = $User->getJWT();

        $User->defineInfoFor($token->id, true);

        $authorId = $db->catchMistakes("SELECT AUTHORID FROM PAGEINFO WHERE ID = ?", $artId)->fetchColumn();

        $size = count($newTag);

        if (!$User->isLoggedIn() ||
            !$User->checkPassword($pass) ||
            ($User->getLevel() < 3 && $User->getId() != $authorId)
            || 3 - $size < 0) {
            return false;
        }

        $newTag = array_merge($newTag, array_fill($size, 3 - $size, null));

        $db->catchMistakes("UPDATE TAGS SET TAG1 = ?, TAG2 = ?, TAG3 = ? WHERE ART_ID = ?", [$newTag[0], $newTag[1], $newTag[2], $artId]);

        return true;
    }

    /**
    * Sets the order articles display in on the main page. Higher the number, the higher on page it should be
    *
    * @param $id - id of article to change
    * @param $num - number the display_order should be set to
    */
    public function setDisplay(int $id, int $num) {

        $db = new MyDB();

        $fId = filter_var($id, FILTER_SANITIZE_STRING);
        $fNum = ($num > -1 && !is_nan($num)) ? $num : 0;

        $db->catchMistakes("UPDATE PAGEINFO SET DISPLAY_ORDER = ? WHERE ID = ?", [$fNum, $fId]);

        return true;
    }
}





?>