<?php

require_once(__DIR__."/../vendor/autoload.php");

class Issue {


    private $num, $name, $madePub, $views, $articles, $isPublic, $settingChanged;


    /**
      * Sets private variables to info of user specified
      *
      * @param $num - issue number
      *
      * @return false if issue by $num doesn't exist, else returns issue name
      */
    public function defineInfoFor(string $num = null) {

        if (!$num) {
            return false;
        }

        $db = new MyDB();

        $filteredIssue = filter_var($num, FILTER_SANITIZE_STRING);


        $allInfo = $db->catchMistakes("SELECT ISSUES.*, SUM(PAGEINFO.VIEWS) AS VIEWS
                                        FROM ISSUES
                                        LEFT JOIN PAGEINFO
                                        ON PAGEINFO.ISSUE = ISSUES.NUM
                                        WHERE ISSUES.NUM = ?
                                         OR
                                        PAGEINFO.ID IN (
                                            SELECT ART_ID FROM TAGS WHERE ? IN (TAG1, TAG2, TAG3)
                                            )",
                                        [$filteredIssue, $filteredIssue]);

        $allInfo->setFetchMode(PDO::FETCH_ASSOC);

        $dbVals = $allInfo->fetch();

        $allInfo->closeCursor();


        if (!$dbVals) {
            return false;
        }


        $articleQuery =
           $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ?", $dbVals["NUM"]);

           $articleIds = $articleQuery->fetchAll(PDO::FETCH_COLUMN, 0);



        $this->num = $dbVals["NUM"];
        $this->name = $dbVals["NAME"];
        $this->madePub = $dbVals["MADEPUB"];
        $this->isPublic = $dbVals["ISPUBLIC"];
        $this->views = $dbVals["VIEWS"];
        $this->articles = $articleIds;

        return $dbVals["NAME"];
    }

    public function route(string $method, array $data) {

        $User = new User();

        if ($method == "PUT") {

            $token = $User->getJWT();
            $User->defineInfoFor($token->id, true);

            if((!$User->checkPassword($data["password"]) || $token->level < 3)) {

                Utilities::setHeader(401);
                return false;
            }
        }


        switch($method) {

            case "GET":
                $Info = new Info();
                return $Info->getIssues();

            case "PUT":
                $this->defineInfoFor($data["issue"]);

                if (array_key_exists("issueName", $data)) {
                    $this->setName($data["issueName"]);
                }

                if (array_key_exists("pub", $data)) {
                    $this->setPublic($data["pub"]);
                }

                Utilities::setHeader(200, "issue updated");
        }
    }


    public function getPreviews(array $data) {

        $User = new User();
        $Info = new Info();

        $maxIssue = ($User->isLoggedIn()) ? max($this->getMax(true), $this->getMax()) : $this->getMax(true);


        $issueNum = (empty($data["issueNum"])) ? $maxIssue : $data["issueNum"];

        // if there's no issue
        if (!$issueNum) {
            Utilities::setHeader(404, "issue");
            exit;
        }

        $this->defineInfoFor($issueNum);

        $toReturn = [];
        $toReturn["articles"] = $Info->getLedesFromIssue($issueNum);
        $toReturn["slides"] = $this->getSlideInfo($issueNum);
        $toReturn["maxIssue"] = $maxIssue;
        $toReturn["name"] = $this->getName();

        return $toReturn;
    }

    /**
      * Saves settings to db if any have been changed in current instantiation of this class
      */
    public function __destruct() {

        if (!$this->settingChanged) {
            return false;
        }

        $db = new MyDB();
        $db->catchMistakes("UPDATE ISSUES SET ISPUBLIC = ?, NAME = ?, MADEPUB = ?
                            WHERE NUM = ?", [$this->isPublic, $this->name, $this->madePub, $this->num]);
    }

    /**
      * @param $issueNum - either number of issue, or a tag
      *
      * @return associative array containing img urls, issue, and article url of articles that have picture in them
      *  and that are in the issue/have the tag
      */
    public function getSlideInfo(string $issueNum) {

        $issueNum = filter_var($issueNum, FILTER_SANITIZE_STRING);

        $db = new MyDB();
        $User = new User();
        $token = $User->getJWT();

        $maxIssue = $this->getMax(true);


        $tagQuery = $db->catchMistakes("SELECT ID FROM TAGS WHERE ? IN (TAG1, TAG2, TAG3)", $issueNum);

        $tagExists = $tagQuery->fetchColumn();

        $tagQuery->closeCursor();


        if ((!$tagExists && preg_match("/^(\d+)|all$/", $issueNum)) || !$issueNum) {

            if ($issueNum > $maxIssue || $issueNum === "") {

                $issueNum = $maxIssue;

                if (isset($token->level)) {

                    $issueNum = $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES")->fetchColumn();
                }
            }

            $slidePrep = $db->catchMistakes("SELECT IMG_URL, URL, ISSUE, SLIDE_IMG FROM PAGEINFO WHERE ISSUE = ?", $issueNum);

            $slidePrep->setFetchMode(PDO::FETCH_ASSOC);
            $slides = $slidePrep->fetchAll();

        }
        else if ($tagExists) {
            $slidePrep = $db->catchMistakes("SELECT IMG_URL, URL, ISSUE, SLIDE_IMG FROM PAGEINFO WHERE ID IN (SELECT ART_ID FROM TAGS WHERE ? IN (TAG1, TAG2, TAG3)) AND ((SELECT ISPUBLIC FROM ISSUES WHERE NUM = ISSUE) OR ?) ORDER BY ISSUE", [$issueNum, $User->isLoggedIn()]);

            $slidePrep->setFetchMode(PDO::FETCH_ASSOC);
            $slides = $slidePrep->fetchAll();
        }

        return (!empty($slides)) ? $slides : [];
    }

    /**
      * Creates new issue. Relies on ISSUE.NUM being autoincremented
      */
    public function create() {

        $db = new MyDB();

        $db->catchMistakes("INSERT INTO ISSUES (NAME) VALUES(NULL)");
    }

    /**
      * @param $isPublic - boolean
      *
      * @return max issue that is public if $isPublic is true, else returns max issue that is private
      */
    public function getMax(bool $isPublic = false) {

        $db = new MyDB();

        if ($isPublic) {
            return $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES WHERE ISPUBLIC = ?", 1)->fetchColumn();
        }
        else {
            return $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES WHERE ISPUBLIC = ?", 0)->fetchColumn();
        }

    }

    /**
      * Changes issue name
      *
      * @param $name - string
      *
      * @return false if $name is invalid acc to $this->validateName, else return true
      */
    public function setName(string $name) {

        if (!$filtName = $this->validateName($name)) {
            return false;
        }

        $this->name = $filtName;
        $this->settingChanged = true;

        return true;
    }

    /**
      * Getter for name
      */
    public function getName() {
        return $this->name;
    }

    /**
      * Checks if $name is valid
      *
      * @param $name - string
      *
      * @return false if $name > 20 chars or article is already public, else returns filtered $name
      */
    private function validateName(string $name) {

        if (strlen($name) > 20 || $this->isPublic) {

            Utilities::setHeader(400, "issue name");
            return false;
        }

        return filter_var($name, FILTER_SANITIZE_STRING);
    }

    /**
      * Makes issue public and sets current date as time issue was made public
      *
      * @return true
      */
    public function setPublic(bool $status = true) {

        if (!$status || strlen($this->name) == 0) {

            Utilities::setHeader(400, "status");
            return false;
        }

        $this->isPublic = true;
        $this->madePub = date("Y-m-d");
        return true;
    }

    /**
      * Getter for isPublic
      */
    public function getPublic() {
        return $this->isPublic;
    }

    /**
      * Getter for date article was made public
      */
    public function getMadePub() {
        return $this->madePub;
    }

    /**
      * Gets number articles in issue
      */
    public function getArticles() {
        return $this->articles;
    }

    /**
      * Gets number of views the issue has
      */
    public function getViews() {
        return $this->views;
    }
}







?>