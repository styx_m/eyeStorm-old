<?php

require_once(__DIR__."/../vendor/autoload.php");



class UserGroup extends User {

    /**
      * Overrides parent class's destructor
      */
    public function __destruct() {

    }

    public function route(string $method, array $data) {

        switch ($method) {

            case "DELETE":
                return $this->delete($data["delAcc[]"], $data["password"]);

            case "PUT":
                $Utilities = new Utilities();

                $sorted = $Utilities->splitArrayByUniq($data["lvl[]"], $data["name[]"]);

                $i = 0;

                while ($i < count($sorted)) {

                    if (!$this->promote($sorted[$i][0], $sorted[$i][1], $data["pass"])) {
                        exit("fail");
                    }

                    $i++;
                }
                return true;

            case "GET":
                $Info = new Info();
                return $Info->getUsersInfo();
        }
    }


    /**
      * Deleted all users whose id is in $idsToDelete
      *
      * @param $idsToDelete - array of existing ids
      * @param $password - password of user
      *
      * @return false if bad password, or user is less than lvl 2. Else return true
      */
    public function delete(array $idsToDelete, string $password) {
        $User = new User();
        $db = new MyDB();
        $Utilities = new Utilities();
        $token = $User->getJWT();


        if (!$User->isLoggedIn()) {

            Utilities::setHeader(401);
            return false;
        }

        $User->defineInfoFor($token->id, true);

        $inClause = $Utilities->generateQuestionMarks(count($idsToDelete));

        $highestLvl = $db->catchMistakes("SELECT MAX(LEVEL) FROM USERS WHERE ID IN {$inClause}", $idsToDelete)->fetchColumn();

        if (!$User->checkPassword($password) || $highestLvl >= $token->level) {

            Utilities::setHeader(401);
            return false;
        }


        $delParams = $idsToDelete;

        array_unshift($delParams, "Deleted");

        $inClause = $Utilities->generateQuestionMarks(count($idsToDelete));

        $db->catchMistakes("UPDATE PAGEINFO SET AUTHORID = (SELECT ID FROM USERS WHERE USERNAME = ?) WHERE AUTHORID IN {$inClause}", $delParams);

        $db->catchMistakes("UPDATE COMMENTS SET AUTHORID = (SELECT ID FROM USERS WHERE USERNAME = ?) WHERE AUTHORID IN {$inClause}", $delParams);

        $db->catchMistakes("DELETE FROM USERS WHERE ID IN {$inClause}", $idsToDelete);

        Utilities::setHeader(200, "user(s) updated");
        return true;
    }

    /**
      * Changes level of all whose users who's usernames are in $usernamesToPromote to level specified by $toLevel
      *
      * @param $usernamesToPromote - valid usernames of existing users
      * @param $toLevel - 1-3, users level will be changed to this
      * @param $password - password of user
      *
      * @return false if bad password or user is lvl 1. Else true
      */
    public function promote(array $usernamesToPromote, int $toLevel, string $password) {

        $Utilities = new Utilities();
        $db = new MyDB();
        $User = new User();
        $token = $User->getJWT();


        if (!$User->isLoggedIn()) {
            Utilities::setHeader(401);
            return false;
        }

        $User->defineInfoFor($token->id, true);

        $inClause = $Utilities->generateQuestionMarks(count($usernamesToPromote));

        $maxPreviousLvl = $db->catchMistakes("SELECT MAX(LEVEL) FROM USERS WHERE USERNAME IN {$inClause}", $usernamesToPromote)->fetchColumn();

        if (!$User->checkPassword($password) || $token->level < 2 || (int) $toLevel > $token->level || (int)$maxPreviousLvl >= $token->level) {

            Utilities::setHeader(401);
            return false;
        }

        $promoteParams = $usernamesToPromote;
        array_unshift($promoteParams, $toLevel);


        $db->catchMistakes("UPDATE USERS SET LEVEL = ? WHERE TRIM(TRAILING '@tabc.org' FROM EMAIL) IN {$inClause}", $promoteParams);

        Utilities::setHeader(200, "user(s) updated");
        return true;
    }

}



?>