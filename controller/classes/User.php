<?php

require_once(__DIR__."/../vendor/autoload.php");
require_once(__DIR__."/../config.php");
use \Firebase\JWT\JWT;
date_default_timezone_set("America/New_York");


// some filter_vars aren't strictly needed, but makes me feel good
class User {

    protected $username, $firstName, $middleName, $lastName, $password, $email, $level,
            $twoFactorEnabled, $notificationsEnabled, $articles, $views, $id,
            $authCode, $authTime, $settingChanged;


    /**
      * Saves settings to db if any have been changed in current instantiation of this class
      */
    public function __destruct() {

        if (!$this->settingChanged) {

            return false;
        }

        $db = new MyDB();

        $db->catchMistakes("UPDATE USERS SET PASSWORD = ?, LEVEL = ?, TWO_FA_ENABLED = ?, EMAIL = ?, NOTIFICATIONS = ?, AUTH = ?,
                            AUTH_TIME = ?, USERNAME = ?
                            WHERE ID = ?",
                            [$this->password, $this->level, $this->twoFactorEnabled, $this->email, $this->notificationsEnabled,
                            $this->authCode, $this->authTime, $this->username, $this->id]);
    }

    /**
      * Creates new account
      * All params will be checked in accordance with their $this->validate function before being entered into database
      * Email is prefixed by a . since has not been verified yet
      *
      * @return false if invalid input, else true
      */
    public function create(string $username, string $fullname, string $password, string $confirmation, string $email, int $level) {

        $allInputValid = $this->validatePassword($password, $confirmation) && $this->validateUsername($username)
                         && $this->validateFullName($fullname) && $this->validateEmail($email) && $this->validateLevel($level);


        if (!$allInputValid || !$this->uniqueEmail($email)) {

            return false;
        }

        $nameArr = $this->validateFullName($fullname);

        $db = new MyDB();

        $db->catchMistakes("INSERT INTO USERS (USERNAME, F_NAME, M_NAME, L_NAME, PASSWORD, LEVEL, EMAIL, NOTIFICATIONS)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                            [$username, $nameArr[0], $nameArr[1], $nameArr[2], password_hash($password, PASSWORD_DEFAULT),
                            $level, ".".$email, 1]);


        $this->email = ".".$email;
        $this->username = $username;
        $this->sendEmailVerification();


        $db->catchMistakes("UPDATE USERS SET AUTH = ?, AUTH_TIME = ? WHERE USERNAME = ?", [$this->authCode, $this->authTime, $username]);

        Utilities::setHeader(201, "user created");

        if (!$this->isLoggedIn()) {
            return $this->login($username, $password);
        }

        return true;
    }

    /**
        * Router for logging user in and out
        */
    public function routeToken(string $method, array $data) {

            if (array_key_exists("authCode", $data) && !empty($data["password"])) {
                
                return $this->login($this->getJWT()->email, $data["password"], $data["authCode"]);
            }


            if (!$this->isLoggedIn() && !empty($data["username"]) && !empty($data["password"])) {

                return $this->login($data["username"], $data["password"]);
            }

            if (isset($data["logout"])) {
                return $this->logout();
            }

            return Utilities::setHeader(422, "missing required field");
    }


    /**
      * Does HTTP REST for directly user related stuff
      *
      * PUT - login OR if already logged in, updates account
      * POST - create
      * GET - user info
      * DELETE - delete user
      *
      *
      */
    public function route(string $method, array $data) {

        if ($method != "POST" && array_key_exists("user", $data) && !$this->exists($data["user"])) {

            Utilities::setHeader(404);
            return false;
        }

        if ($method != "GET" && $method != "POST" && !array_key_exists("lastAuth", $data)) {
            $this->defineInfoFor($this->getJWT()->id, true);
        }
        else if ($method == "GET" || array_key_exists("lastAuth", $data)) {
            $this->defineInfoFor($data["user"]);
        }



        switch ($method) {

            case "GET":
                return $this->getAllUserInfo();

            case "PUT":
                return $this->changeSettingsWhenRouting($data);

            case "DELETE":
                return $this->destroy($data["delAcc"]);

            case "POST":
                return $this->create(...array_values($data));
        }
    }

    private function getAllUserInfo() {

        $token = $this->getJWT();

        $db = new MyDB();
        $toReturn = [["USERNAME" => ($this->id == $token->id) ? $this->getUsername() : null, "NAME" => $this->getFullName(), "LEVEL" => $this->getLevel(),
                        "ARTICLES" => $this->getArticleNum(), "VIEWS" => $this->getViews()
                        ]];

        $artQuery = $db->catchMistakes("SELECT URL, CREATED, CONCAT(TAG1, IFNULL(CONCAT(', ', TAG2), ''), IFNULL(CONCAT(', ', TAG3), '')) AS TAGS, VIEWS,
                    PAGEINFO.ID AS ART_ID, ISSUE
                    FROM PAGEINFO
                    LEFT JOIN ISSUES
                    ON PAGEINFO.ISSUE = ISSUES.NUM
                    JOIN TAGS
                    ON TAGS.ART_ID = PAGEINFO.ID
                    WHERE AUTHORID = (SELECT ID FROM USERS WHERE USERNAME = ?) AND (ISPUBLIC = 1 OR ?)
                    ORDER BY ISSUE, URL", [$this->username, $this->isLoggedIn()]);

        $artQuery->setFetchMode(PDO::FETCH_ASSOC);

        $toReturn[] = $artQuery->fetchAll();

        if ($this->isLoggedIn() && $token->id == $this->id) {

            $toReturn[] = ["notificationStatus" => $this->getNotificationStatus(), "twoFactor" => $this->getTwoFactor(),
                           "email" => $this->email, "emailConfirmed" => !!$this->validateEmail($this->email), "id" => $this->id];
        }


        return $toReturn;
    }

    private function changeSettingsWhenRouting(array $data) {

        if (array_key_exists("lastAuth", $data)) {

            return $this->forgotPassword($data["email"], $data["lastAuth"]);
        }

        else if (array_key_exists("userEmail", $data) && $this->checkPassword($data["password"])) {

            return $this->setEmail($data["userEmail"]) &&
            $this->setTwoFactor($data["2fa"]) &&
            $this->setNotificationStatus($data["notifications"]);

        }

        else if ($this->checkPassword($data["password"])) {

            return $this->setPassword($data["newPass"], $data["passConf"]);
        }


    }

    private function uniqueEmail(string$email) {

        $db = new MyDB();

        $duplicate = $db->catchMistakes("SELECT EMAIL FROM USERS WHERE EMAIL = ? OR EMAIL = ?", [".".$email, $email])->fetchColumn();


        if (!$duplicate || strpos($email, "meiseles") !== false) { // lets me have many accounts with same email
            return true;
        }

        Utilities::setHeader(409, "email");
        return false;
    }

    /**
      * Removes account from database, replacing all PAGEINFO.AUTHORID's with the id of Deleted User
      *  Also sets all instance variables to null
      *
      * @param $id - id of user to destroy
      */
    public function destroy(int $id) {

        $db = new MyDB();
        $token = $this->getJWT();

        if ($id != $token->id && $token->level <= $db->catchMistakes("SELECT LEVEL FROM USERS WHERE ID = ?", $id)->fetchColumn()) {
            return false;
        }


        $deletedUserId = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME = ?", "Deleted")->fetchColumn();

        $db->catchMistakes("UPDATE PAGEINFO SET AUTHORID = ? WHERE AUTHORID = ?", [$deletedUserId, $id]);

        $db->catchMistakes("UPDATE COMMENTS SET AUTHORID = ? WHERE AUTHORID = ?", [$deletedUserId, $id]);
        $db->catchMistakes("DELETE FROM USERS WHERE ID = ?", $id);

        $this->settingChanged = false;

        $this->username = $this->firstName = $this->middleName = $this->lastName = $this->password = $this->email =
        $this->level = $this->twoFactorEnabled = $this->notificationsEnabled = $this->publicArticlesNum = $this->allArticlesNum = $this->views =
        $this->id = $this->settingChanged = null;

        return true;

    }

    /**
      * Logs in user
      *
      * @param $username - username in db
      * @param $password - password that matches $username
      * @param $code - if 2fa code is given, this will be it
      *
      * @return true if user has been successfully logged in, 0 if 2fa is needed, else returns false
      */
    public function login(string $username, string $password, string $code = "") {

        $userId = $this->defineInfoFor($username);

        $token = $this->getJWT();


        if (!$this->checkPassword($password)) {
            Utilities::setHeader(400, "password");
            return false;
        }

        // get email. Match .words2018@tabc.org, name.nextName@tabc.org, etc and only takes words@2018 or name.nextName
        preg_match("/\w+\.?\d?\w+/", $this->email, $result);
        $emailInfo = $result[0];

        $this->changeJWT(["email" => $emailInfo]);

        if ($this->email[0] == ".") {


            if (!$this->checkAuth($code)) {

                if ($code) { // if user actually put in a code (as opposed to this being called from $this->create)
                    Utilities::setHeader(400, "auth code");
                    return;
                }

                Utilities::setHeader(200, "email sent");
                return;
            }

            $this->makeEmailUsable();
        }
        // the check for $_SESSION["user] is to make sure not to send another email every time page is refreshed
        if ($this->twoFactorEnabled && !$this->checkAuth($code)) {

            if (!empty($token->id)) {

                Utilities::setHeader(202);
                exit;
            }

            $this->changeJWT(["id" => $this->id]);  // put in 2 places to not send many emails if refresh auth page

            $code = $this->set2FAInfo(date('Y-m-d H:i:s', strtotime("+15 minutes")));
            $SendMail = new SendMail();

            return ($SendMail->twoFactor($this->email, $code)) ? 0 : false;
        }

        $this->changeJWT([
            "email" => $emailInfo,
            "level" => $this->level,
            "id" => $this->id
            ]);

        return true;
    }

    /**
      * Sets jwt payload to values specified by function params
      *
      * @param $username - corresponds to "user" field
      * @param $level - refers to "level" field
      * @param $id - "id" field
      */
    public function changeJWT(array $fieldValues = []) {


        $prevValues = $this->getJWT();

        $token = [
            [
                "iss" => "https://tabceots.com",
                "iat" => time()
            ],
            [
                "email" => isset($fieldValues["email"]) ? $fieldValues["email"] : $prevValues->email ?? null,
                "level" => isset($fieldValues["level"]) ? $fieldValues["level"] : $prevValues->level ?? null,
                "id" => isset($fieldValues["id"]) ? $fieldValues["id"] : $prevValues->id ?? null,
                "automatedTest" => isset($fieldValues["automatedTest"])
            ]
        ];

        $jwt = JWT::encode($token, JWT_SECRET);

        if (array_key_exists("automatedTest", $token[1]) && $token[1]["automatedTest"]) {
            $_COOKIE["jwt"] = $jwt; // can't set cookies if no browser so set superglobal instead
        }

        else {
            setcookie("jwt", $jwt, null, "/");
        }
    }


    public function getJWT() {

        $token = (isset($_COOKIE["jwt"])) ? JWT::decode($_COOKIE["jwt"], JWT_SECRET, array('HS256'))[1] : null;

        return $token;
    }

    public function getJWTHeaders() {

        return (isset($_COOKIE["jwt"])) ? JWT::decode($_COOKIE["jwt"], JWT_SECRET, array('HS256'))[0] : false;
    }

    /**
      * Sets instance variables to info of user specified
      *
      * @param $whereClause must be email (not including anything past and including @) of existing account in db, or id of exisitng account
      * @param $isId - if true, the whereClause is assumed to be an id, else assumed to be username
      *
      * @return false if nonexistant user, or with id of user if successful
      */
    public function defineInfoFor(string $whereClause, bool $isId = false) {

        $db = new MyDB();

        $filtWhereClause = filter_var($whereClause, FILTER_SANITIZE_STRING);

       if (!$isId && !preg_match("/^\d+$/", $filtWhereClause)) {

            $allInfo =
            $db->catchMistakes("SELECT USERS.*, COUNT(PAGEINFO.ID) AS ARTICLES, IFNULL(SUM(VIEWS), 0) AS VIEWS
                                FROM USERS
                                LEFT JOIN ISSUES ON (ISPUBLIC = 1 OR ?)
                                LEFT JOIN PAGEINFO ON ISSUE = NUM AND AUTHORID = USERS.ID
                                WHERE USERNAME = ? OR TRIM(LEADING '.' FROM TRIM(TRAILING '@tabc.org' FROM USERS.EMAIL)) = ? LIMIT 1", [$this->isLoggedIn(), $filtWhereClause, $filtWhereClause]);
        }
        else {

            $allInfo =
            $db->catchMistakes("SELECT USERS.*, COUNT(PAGEINFO.ID) AS ARTICLES, IFNULL(SUM(VIEWS), 0) AS VIEWS
                                FROM USERS
                                LEFT JOIN ISSUES ON (ISPUBLIC = 1 OR ?)
                                LEFT JOIN PAGEINFO ON ISSUE = ISSUES.NUM AND AUTHORID = ?
                                WHERE USERS.ID = ?", [$this->isLoggedIn(), $filtWhereClause, $filtWhereClause]);
        }


        $allInfo->setFetchMode(PDO::FETCH_ASSOC);

        $dbVals = $allInfo->fetch();

        $allInfo->closeCursor();

        if (!$dbVals) {
            return false;
        }

        $this->username = $dbVals["USERNAME"];
        $this->setFullName("{$dbVals["F_NAME"]} {$dbVals["M_NAME"]} {$dbVals["L_NAME"]}");
        $this->password = $dbVals["PASSWORD"];
        $this->id = $dbVals["ID"];
        $this->email = $dbVals["EMAIL"];
        $this->level = $dbVals["LEVEL"];
        $this->setTwoFactor($dbVals["TWO_FA_ENABLED"]);
        $this->setNotificationStatus($dbVals["NOTIFICATIONS"]);
        $this->articles = $dbVals["ARTICLES"];
        $this->views = $dbVals["VIEWS"];
        $this->authCode = $dbVals["AUTH"];
        $this->authTime = $dbVals["AUTH_TIME"];

        $this->settingChanged = false;

        return $dbVals["ID"];
    }

    /**
      * Logs out user
      */
    public function logout() {

        $_COOKIE["jwt"] = null;
        setcookie("jwt", "", time() - 1, "/");
    }

    /**
      * @return true if this instance of user is logged in, else false
      */
    public function isLoggedIn() {

        $token = $this->getJWT();
        return isset($token) && isset($token->level) && !empty($token->email) && !empty($token->id);
    }

    public function exists(string $username) {

        $db = new MyDB();
        return !!$db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME = ?
        OR TRIM(TRAILING '@tabc.org' FROM EMAIL) = ?", [$username, $username])->fetchColumn();
    }

    /**
      * If $username is valid according to $this->validateUsername, sets $this->username as $username
      *
      * @param username - potential username
      *
      * @return true if $username is set as new username, else false
      */
    public function setUsername(string $username) {

        if (!$newUsername = $this->validateUsername($username)) {
            return false;
        }


        $this->username = $newUsername;


        $this->settingChanged = true;
        return true;
    }

    /**
      * @param $username - to be evaluated
      *
      * @return validated and sanitized username if string of < 20 chars containing no spaces, else else
      */
    public function validateUsername(string $username) {

        if (strlen($username) > 20 || preg_match("/\s/", $username) || strlen($username) < 1) {

            Utilities::setHeader(400, "username");
            return false;
        }

        return filter_var($username, FILTER_SANITIZE_STRING);
    }

    public function getUsername() {
        return $this->username;
    }

    /**
      * Sets the name of user by splitting input into first, last, and middle name
      *
      * @param $name - potential new name
      *
      * @return true if $name is in accordance to $this->validateFullName, else false
      */
    public function setFullName(string $name) {

        if (!$newName = $this->validateFullName($name)) {

            return false;
        }

        list($this->firstName, $this->middleName, $this->lastName) = $newName;

        $this->settingChanged = true;

        return true;
    }

    /**
      * @param $name - to be checked
      *
      * @return false if not 2-3 words, first having > 10 chars, 2nd (if provided) > 3, or 3rd > 20. Else return sanitized string
      */
    public function validateFullName(string $name) {

        // word space word OR space word
        if (!preg_match("/^[a-zA-Z.-]+\s?[a-zA-Z.-]{0,3}\s[a-zA-Z.-]+$/", $name)) {
            Utilities::setHeader(400, "name");
            return false;
        }

        $nameArr = explode(" ", filter_var($name, FILTER_SANITIZE_STRING));

        if (count($nameArr) < 3) {

            array_splice($nameArr, 1, 0, "");
        }

        if (strlen($nameArr[0]) > 10 || strlen($nameArr[1]) > 3 || strlen($nameArr[2]) > 20) {
            Utilities::setHeader(400, "name");
            return false;
        }

        return $nameArr;
    }

    /**
      * @return user's first name
      */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
      * @return user's middle name (or empty string if none)
      */
    public function getMiddleName() {
        return $this->middleName;
    }

    /**
      * @return user's last name
      */
    public function getLastName() {
        return $this->lastName;
    }

    /**
      * @return user's full name. Each part of name is separated by a space
      */
    public function getFullName() {
        return $this->firstName . " ".$this->middleName . " ".$this->lastName;
    }

    /**
      * Sets $this->password as hashed version of $password
      *
      * @param $password - string
      * @param $confirmation - must be same as $password
      *
      * @return true if $password valid according to $this->validatePassword, else false
      */
    public function setPassword(string $password, string $confirmation) {

        if (!$newPassword = $this->validatePassword($password, $confirmation)) {
            return false;
        }


        $this->password = password_hash($newPassword, PASSWORD_DEFAULT);
        $this->settingChanged = true;
        return true;
    }

    /**
      * @param $password - to check
      *
      * @return false if $password does not contain either at least 1 upper, lower, digit and at least 6 chars, or is less than 15 chars,
      * else returns filtered $password
      */
    protected function validatePassword(string $password, string $confirmation) {

        if ($password != $confirmation || (!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*).{6,}$/", $password) && strlen($password) < 15)) {
            Utilities::setHeader(400, "password");
            return false;
        }

        return filter_var($password, FILTER_SANITIZE_STRING);
    }

    /**
      * @param $passwordGiven - to check
      *
      * @return true if $passwordGiven is verified according to $this->password else false
      */
    public function checkPassword(string $passwordGiven) {
        return password_verify($passwordGiven, $this->password);
    }

    /**
      * Sets $this->email. If successful, also email user a verification code
      *
      * @param $email - potential new email of user
      *
      * @return true if $email valid according to $this->validateEmail, else false
      */
    public function setEmail(string $email) {

         if (!$newEmail = $this->validateEmail($email)) {
             return false;
         }

         if ($newEmail == $this->email) {
             return true;
         }

         $this->email = ".".$newEmail;

         $this->sendEmailVerification();

         $this->settingChanged = true;

         return true;
    }

    /**
      * @param $email - to be checked
      *
      * @return false if invalid email format or doesn't end with (4 digits or word.word) and @tabc.org, else return filtered email
      */
    public function validateEmail(string $email) {

         if ((!filter_var($email, FILTER_VALIDATE_EMAIL) && !filter_var(substr($email, 1), FILTER_VALIDATE_EMAIL))
          || !preg_match("/^(?:(?:\w+\d{4})|(?:[a-z]+\.[a-z]+))@tabc\.org$/", $email)) {

            Utilities::setHeader(400, "email");
            return false;
        }

        return filter_var($email, FILTER_SANITIZE_EMAIL);
    }

    /**
      * @return $this->email
      */
    public function getEmail() {
        return $this->email;
    }

    /**
      * If $this->email starts with a dot (.) as all new emails are, this removes it
      *
      * @return true
      */
    public function makeEmailUsable() {

        if ($this->email[0] == ".") {
            $this->email = substr($this->email, 1);
            $this->settingChanged = true;
        }
        return true;
    }

    /**
      * Sets level of user to $lvl if successful
      *
      * @param $lvl - number
      *
      * @return true if $lvl in accordance with $this->validateLevel, else false
      */
    public function setLevel(int $lvl) {

        if (!$newLvl = $this->validateLevel($lvl)) {
            return false;
        }


        $this->level = $newLvl;
        $this->settingChanged = true;

        if ($this->isLoggedIn()) {

           $this->level = $newLvl;
        }

        return true;
    }

    /**
      * @param $lvl potential level of user
      *
      * @return filtered $lvl if $lvl is 1-3, else false
      */
    public function validateLevel(int $lvl) {

        if (!preg_match("/^[1-3]$/", $lvl) || $lvl > max($this->getJWT()->level, 1)) {

            Utilities::setHeader(400, "level");
            return false;
        }

        return filter_var($lvl, FILTER_SANITIZE_NUMBER_INT);

    }

    /**
      * @return $this->level
      */
    public function getLevel() {

        return $this->level;
    }

    /**
      * Sets two factor authentication setting
      *
      * @return true
      */
    public function setTwoFactor(bool $bool = null) {

        // doing ternary instead of directly assigning $bool to prevent bad input
        $this->twoFactorEnabled = ($bool) ? 1 : 0;
        $this->settingChanged = true;

        return true;
    }

    /**
      * @return $this->twoFactorEnabled
      */
    public function getTwoFactor() {

        return $this->twoFactorEnabled;
    }

    /**
      * Sets notification setting
      *
      * @return true
      */
    public function setNotificationStatus(bool $bool = null) {
        $this->notificationsEnabled = ($bool) ? 1 : 0;
        $this->settingChanged = true;

        return true;
    }

    /**
      * @return $this->notificationsEnabled
      */
    public function getNotificationStatus() {
        return $this->notificationsEnabled;
    }


    /**
      * @return $this->articles; num of public articles user published. If user is not logged in, can only see number that is public
      */
    public function getArticleNum() {

         return $this->articles;
     }


    /**
      * @return $this->views; how many people viewed article published by user
      */
    public function getViews() {
        return $this->views;
    }

    /**
      * @param $time - time authcode was generated
      *
      * @return true
      */
    public function set2FAInfo(string $time) {

        $code = bin2hex(openssl_random_pseudo_bytes(6));

        $this->authCode = password_hash($code, PASSWORD_DEFAULT);
        $this->authTime = $time;

        $this->settingChanged = true;

        return $code;
    }

    /**
      * Sends verification email (typically when user's email is changed)
      */
    public function sendEmailVerification() {

        $SendMail = new SendMail();

        $code = $this->set2FAInfo(date('Y-m-d H:i:s', strtotime("+1 day")));

        $SendMail->emailAuth($this->email, $this->username, $code);

        return true;
    }


    /**
      * @return true if correct auth code and within 15 minutes of it being generated, else false
      */
    public function checkAuth(string $toCheck, bool $timeMatters = true) {

        return password_verify($toCheck, $this->authCode) && (!$timeMatters || strtotime($this->authTime) - time() > 0);
    }

    /**
      * Changes user's password to randomly generated and sends user email containing new password
      *
      * @param $email - user's email
      * @param $lastAuth - last auth code the user used
      *
      * @return true if successful, else false
      */
    public function forgotPassword(string $email, string $lastAuth) {

        $newPassword = bin2hex(openssl_random_pseudo_bytes(30));

        if (!$this->checkAuth($lastAuth, false) || $email != $this->email || !$this->setPassword($newPassword, $newPassword)) {
            return false;
        }

        Utilities::setHeader(200, "email sent");

        $SendMail = new SendMail();
        return $SendMail->passwordRecovery($newPassword, $this->username, $this->email);
    }

    /**
      * @return user's id
      */
    public function getId() {
        return $this->id;
    }

}

?>