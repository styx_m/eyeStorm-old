<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class RouterTest extends TestCase {

    public static function setUpBeforeClass() {
        HelpTests::setupForTests();
    }


    /***GET***/

    /**
      * Gets comments
      *
      * @param $issue - issue number of article
      * @param $url - url of article
      * @param $lvl - lvl of user trying to get comments
      * @param $loggedIn - boolean
      *
      * @return boolean if comments were successfully gotten for article specified
      */
    private function helpTestGetComments($public, $url, $lvl = 1, $loggedIn = true) {

        $db = new MyDB();

        $issue = ($public) ? HelpTests::$pubIssue : HelpTests::$privIssue;

        $user = HelpTests::createNewUser($lvl);
        HelpTests::helpSetSessions($user, $lvl, $loggedIn);

        $res = (array) HelpTests::createHTTPRequest("comment", ["issue" => $issue, "url"=>$url], "GET");

        HelpTests::deleteNewUser();

        if ($loggedIn || $public) {
            $commentQuery = $db->catchMistakes("SELECT COMMENTS.ID, AUTHORID,
                                        CONCAT(F_NAME, ' ', IFNULL(M_NAME, ''), ' ', L_NAME) AS AUTHOR_NAME,
                                        USERNAME, CONTENT, CREATED
                                        FROM COMMENTS
                                        LEFT JOIN USERS ON AUTHORID = USERS.ID
                                        WHERE ART_ID = (SELECT ID FROM PAGEINFO WHERE ISSUE = ? AND URL = ?)", [$issue, $url]);
            $commentQuery->setFetchMode(PDO::FETCH_ASSOC);
            $comments = $commentQuery->fetchAll();


            for ($i = 0; $i < max(count($res), count($comments)); $i++) {
                if ((array)$res[$i] != $comments[$i]) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    public function testBadGetCommentsPrivateArticleNotLoggedIn() {

        $this->assertFalse($this->helpTestGetComments(false, HelpTests::$url[3], 1, false));
    }

    public function testGoodGetCommentsPublicArticleNotLoggedIn() {

        $this->assertTrue($this->helpTestGetComments(true, HelpTests::$url[0], 1, false));
    }

    public function testGoodGetCommentsPublicArticleYesLoggedIn() {

        foreach([1, 2, 3] as $lvl) {

            $this->assertTrue($this->helpTestGetComments(true, HelpTests::$url[0], $lvl));
        }
    }

    // not stricty needed since will make it so can't comment before article is public anyways
    public function testGoodGetCommentsPrivateArticleYesLoggedIn() {

        foreach([1, 2, 3] as $lvl) {

            $this->assertTrue($this->helpTestGetComments(false, HelpTests::$url[3], $lvl));
        }
    }


    /***POST***/

    /**
      * Checks if user can create a valid comment for an article
      *
      * @param $public - status of article
      * @param $content - comment text
      * @param $lvl - level of commenter
      * @param $loggedIn - boolean commenter logged in
      */
    private function helpTestCreateComments($public, $content = "Good job", $lvl = 1, $loggedIn = true) {

        $db = new MyDB();
        $issue = ($public) ? HelpTests::$pubIssue : HelpTests::$privIssue;
        $url = ($public) ? HelpTests::$url[0] : HelpTests::$url[3];
        $artId = ($public) ? HelpTests::$pubId : HelpTests::$privId;

        $user = HelpTests::createNewUser($lvl);

        HelpTests::helpSetSessions($user, $lvl, $loggedIn);

        $res = HelpTests::createHTTPRequest("comment", [
                "issue" => $issue,
                "url" => $url,
                "content" => $content
            ], "POST");

        $commentQuery = $db->catchMistakes("SELECT CONTENT, CREATED, ART_ID FROM COMMENTS WHERE AUTHORID = ?", $user["id"]);
        $commentQuery->setFetchMode(PDO::FETCH_NUM);
        $comment = $commentQuery->fetchAll();

        $db->catchMistakes("DELETE FROM COMMENTS WHERE AUTHORID = ?", $user["id"]);
        HelpTests::deleteNewUser();

        return count($comment) == 1 && $comment[0] == [$content, date("Y-m-d H:i:s"), $artId];
    }


    public function testBadCommentNotLoggedIn() {

        $this->assertFalse($this->helpTestCreateComments(true, "Best content", 1, false));
    }

    public function testBadCommentLevelThreePrivateArticle() {

        $this->assertFalse($this->helpTestCreateComments(false, "Best content", 3));
    }

    public function testBadCommentTooShort() {

        $this->assertFalse($this->helpTestCreateComments(true, "Hi"));
    }

    public function testBadCommentUnsafeContent() {

        foreach (HelpTests::$unsafeStrings as $str) {

            $this->assertFalse($this->helpTestCreateComments(true, $str));
        }
    }

    public function testBadCommentNonexistantArticle() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $res = HelpTests::createHTTPRequest("comment", [
                "issue" => HelpTests::$pubIssue,
                "url" => "nonexistant",
                "content" => "Best content"
            ], "POST");

        $this->assertNull($res);
    }


    public function testGoodCommentPublicArticleAnyLevel() {
        foreach([1, 2, 3] as $lvl) {

            $this->assertTrue($this->helpTestCreateComments(true, "Some content", $lvl), "Failed when level is {$lvl}");
        }
    }


    /****DELETE***/

    /**
      * Creates a comment with info given, then returns whether the attempt to delete it through api was successful
      *
      * @param $lvl - level of user who's deleting comment
      * @param $author - boolean if deleting own comment
      * @param $loggedIn - if user is logged in
      */
    private function helpTestDeleteComments($lvl = 3, $author = false, $loggedIn = true) {

        $db = new MyDB();
        $commenter = HelpTests::createNewUser($lvl, "commenter");

        $deleter = ($author) ? $commenter : HelpTests::createNewUser($lvl, "deleter");

        HelpTests::helpSetSessions($deleter, $lvl, $loggedIn);
        $content = "test comment delete";

        $db->catchMistakes("INSERT INTO COMMENTS (ART_ID, AUTHORID, CONTENT, CREATED) VALUES(?, ?, ?, CURDATE())",
                             [HelpTests::$pubId, $commenter["id"], $content]);

        $commentId = $db->catchMistakes("SELECT ID FROM COMMENTS WHERE AUTHORID = ?",
                       $commenter["id"])->fetchColumn();

        HelpTests::createHTTPRequest("comment", ["id" => $commentId], "DELETE");

        $successQuery = $db->catchMistakes("SELECT CONTENT, AUTHORID FROM COMMENTS WHERE ID = ?", $commentId);
        $deletedId = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME = ?", "Deleted")->fetchColumn();

        $successQuery->setFetchMode(PDO::FETCH_NUM);
        $success = $successQuery->fetchAll()[0];


        $db->catchMistakes("DELETE FROM COMMENTS WHERE ID = ?", $commentId);
        HelpTests::deleteNewUser($commenter["username"]);
        HelpTests::deleteNewUser($deleter["username"]);

        return $success == ["deleted", $deletedId];
    }

    public function testBadDeleteNotLoggedIn() {

        $this->assertFalse($this->helpTestDeleteComments(1, false, false));
    }

    public function testBadDeleteNotLevelThreeOrCommentAuthor() {

        foreach([1, 2] as $lvl) {

            $this->assertFalse($this->helpTestDeleteComments($lvl, false));
        }
    }

    public function testGoodDeleteCommentAuthor() {

        foreach([1, 2, 3] as $lvl) {

            $this->assertTrue($this->helpTestDeleteComments($lvl, true));
        }
    }

    public function testGoodDeleteLevelThree() {

        $this->assertTrue($this->helpTestDeleteComments(3));
    }


    public static function tearDownAfterClass() {
        HelpTests::returnToNormal();
    }
}
?>