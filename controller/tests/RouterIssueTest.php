<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class RouterIssueTest extends TestCase {

    public static function setUpBeforeClass() {
        HelpTests::setupForTests();
    }


    /***GET***/

    public function testGetIssuesNotLoggedIn() {

        HelpTests::endSession();
        $res = HelpTests::createHTTPRequest("issue", [], "GET");


        $db = new MyDB();
        $issueInfoQuery = $db->catchMistakes("SELECT NUM, IFNULL(NAME, 'N/A') AS NAME, IFNULL((SELECT SUM(VIEWS) FROM PAGEINFO WHERE ISSUE = NUM), 0) AS VIEWS, MADEPUB
                                                FROM ISSUES
                                                WHERE ISPUBLIC = 1
                                                ORDER BY NUM DESC");

        $issueInfoQuery->setFetchMode(PDO::FETCH_OBJ);
        $issueInfo = $issueInfoQuery->fetchAll();

        $this->assertEquals($issueInfo, $res);
    }

    public function testGetIssuesLoggedIn() {

        $res = HelpTests::createHTTPRequest("issue", [], "GET");

        HelpTests::changeSessions(HelpTests::$test1Id, 1, "test1");

        $db = new MyDB();
        $issueInfoQuery = $db->catchMistakes("SELECT NUM, IFNULL(NAME, 'N/A') AS NAME, IFNULL((SELECT SUM(VIEWS) FROM PAGEINFO WHERE ISSUE = NUM), 0) AS VIEWS, MADEPUB
                                                FROM ISSUES
                                                ORDER BY NUM DESC");

        $issueInfoQuery->setFetchMode(PDO::FETCH_OBJ);
        $issueInfo = $issueInfoQuery->fetchAll();

        $this->assertEquals($issueInfo, $res);
    }


    /**PUT***/

    /**
      * @param $lvl - level of user making public
      * @param $pubStatus - boolean if issue already public or not
      * @param $isLoggedIn - boolean if user's logged in
      * @param $issueName - name of issue
      *
      * @return boolean if issue has been made public and `madepub` has been set to date made public
      */
    private function helpTestIssuePublic($pubStatus, $lvl = 3, $isLoggedIn = true, $issueName = "name") {

        $db = new MyDB();

        $issue = HelpTests::$privIssue + 1;
        $user = HelpTests::createNewUser($lvl);

        $db->catchMistakes("INSERT INTO ISSUES (NUM) VALUES(?)", $issue);

        HelpTests::helpSetSessions($user, $lvl, $isLoggedIn);

        HelpTests::createHTTPRequest("issue", [
                                            "issue"=> $issue,
                                            "pub" => $pubStatus,
                                            "issueName" => $issueName,
                                            "password"=>HelpTests::$adminPass
                                            ],
                                  "PUT");

        $resultsQuery = $db->catchMistakes("SELECT ISPUBLIC, MADEPUB FROM ISSUES WHERE NUM = ?", $issue);
        $resultsQuery->setFetchMode(PDO::FETCH_ASSOC);
        $results = $resultsQuery->fetchAll()[0];

        $db->catchMistakes("DELETE FROM ISSUES WHERE NUM > ?", HelpTests::$privIssue);
        $db->catchMistakes("ALTER TABLE ISSUES AUTO_INCREMENT = ".HelpTests::$privIssue);
        HelpTests::deleteNewUser();

        return !!($results["ISPUBLIC"] == 1 && $results["MADEPUB"] == date("Y-m-d"));
    }



    public function testBadSetPublicNotLoggedIn() {

        $this->assertFalse($this->helpTestIssuePublic(true, 1, false));
    }

    public function testBadSetPublicNotLevelThree() {

        foreach([1, 2] as $lvl) {

            $this->assertFalse($this->helpTestIssuePublic(true, $lvl));
        }
    }

    public function testGoodSetPublicLevelThree() {

        $this->assertTrue($this->helpTestIssuePublic(true, 3));
    }

    // make sure date doesn't change if set public after already public
    public function testBadSetPublicTwice() {

        $db = new MyDB();

        $madePubBefore = $db->catchMistakes("SELECT MADEPUB FROM ISSUES WHERE NUM = ?", HelpTests::$pubIssue)->fetchColumn();

        HelpTests::createHTTPRequest("issue", ["issue"=> HelpTests::$pubIssue, "pub" => true, "password"=>HelpTests::$adminPass], "PUT");

        $madePubTwo = $db->catchMistakes("SELECT MADEPUB FROM ISSUES WHERE NUM = ?", HelpTests::$pubIssue)->fetchColumn();

        $this->assertEquals($madePubBefore, $madePubTwo);
    }

    public function testBadSetPublicThenPrivate() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        HelpTests::createHTTPRequest("issue", ["issue"=> HelpTests::$pubIssue, "pub" => false, "password"=>HelpTests::$adminPass], "PUT");

        $db = new MyDB();
        $isPub = !!$db->catchMistakes("SELECT ISPUBLIC FROM ISSUES WHERE NUM = ?", HelpTests::$pubIssue)->fetchColumn();

        $this->assertTrue($isPub);

        $db->catchMistakes("UPDATE ISSUES SET ISPUBLIC = ? WHERE NUM = ?", [true, HelpTests::$pubIssue]); // just in case
    }

    public function testBadSetPublicNoName() {

        $this->assertFalse($this->helpTestIssuePublic(true, 3, true, null));
    }


    /**
      *
      * @param $name - string to name issue
      * @param $lvl - level of user trying to set name
      * @param $issue - issue number
      * @param $isLoggedIn - if user's logged in
      *
      * @return boolean if issue name matches what it's been set to
      */
    private function helpTestIssueName($name, $pubStatus, $lvl = 3, $isLoggedIn = true) {

        $db = new MyDB();

        $issue = ($pubStatus) ? HelpTests::$pubIssue : HelpTests::$privIssue;
        $user = HelpTests::createNewUser($lvl);
        HelpTests::helpSetSessions($user, $lvl, $isLoggedIn);

        HelpTests::createHTTPRequest("issue", ["issue"=>$issue, "issueName"=>$name, "password"=>HelpTests::$adminPass], "PUT");

        $newName = $db->catchMistakes("SELECT NAME FROM ISSUES WHERE NUM = ?", $issue)->fetchColumn();

        $db->catchMistakes("UPDATE ISSUES SET NAME = ? WHERE NUM = ?", ["testIssueName{$pubStatus}", $issue]);
        HelpTests::deleteNewUser();

        return $newName == $name;
    }


    public function testBadSetNameNotLoggedIn() {

        $this->assertFalse($this->helpTestIssueName("name", false, 1, false));
    }

    public function testBadSetNameNotLevelThree() {

        foreach([1, 2] as $lvl) {

            $this->assertFalse($this->helpTestIssueName("name", false, $lvl));
        }
    }

    public function testBadSetNameOfPublicIssue() {

        $this->assertFalse($this->helpTestIssueName("name", true));
    }

    public function testBadSetNameUnsafeString() {

        foreach(HelpTests::$unsafeStrings as $str) {

            $this->assertFalse($this->helpTestIssueName($str, false));
        }
    }

    public function testBadSetNameTooLong() {

        $this->assertFalse($this->helpTestIssueName("1234567890qwertyuiopa", false));
    }

    public function testBadSetNameTooShort() {

        $this->assertFalse($this->helpTestIssueName("", false));
    }

    public function testGoodSetNameLevelThree() {

        $this->assertTrue($this->helpTestIssueName("name", false));
    }




    public static function tearDownAfterClass() {
        HelpTests::returnToNormal();
    }
}




?>