<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


HelpTests::changeSessions(HelpTests::$adminId, 3, "BobbyA");


class GroupTest extends TestCase {

    public static function setUpBeforeClass() {

        $db = new MyDB();

        HelpTests::setupForTests();

        $usersToCreate = "";

        $hashedBobbyPass = password_hash("aS3df 4d", PASSWORD_DEFAULT);

        for ($i = 0; $i < 10; $i++) {

            $k = chr($i + 65); // to circumvent requirements that certain fields be unique

            $usersToCreate .= '("Bobby'.$k.'", "Hi", "Bye", "Bobson '.$k.'", "'.$hashedBobbyPass.'", 1, "meiseles{$k}d2018@tabc.org", 1),';
        }

        $usersToCreate = substr($usersToCreate, 0, strlen($usersToCreate) - 1); // gets rid of comma at the end

        $db->catchMistakes("INSERT INTO USERS (USERNAME, F_NAME, M_NAME, L_NAME, PASSWORD, LEVEL, EMAIL, NOTIFICATIONS) VALUES {$usersToCreate}");
    }

    public function testMassPromotion() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, HelpTests::$adminEmail);

        $db = new MyDB();
        $UserGroup = new UserGroup();

        $ids = $this->getBobbys();

        $this->assertTrue($UserGroup->promote($ids, 2, "a97A65"));

        $allLvls = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME LIKE 'Bobby%'");
        $lvls = $allLvls->fetchAll(PDO::FETCH_COLUMN, 0);


        foreach ($lvls as $lvl) {
            $this->assertEquals(2, $lvl);
        }
    }

    public function testInvalidMassPromotionLowLevel() {

        HelpTests::changeSessions(HelpTests::$test1Id, 1, HelpTests::$test1Email);

        $this->promoteFail();
    }

    public function testInvalidMassPromotionNotLoggedIn() {

        HelpTests::endSession();

        $this->promoteFail();
    }

    public function testInvalidMassPromotionIncorrectPassword() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, HelpTests::$adminEmail);
        $this->promoteFail("bad password");
    }


    public function testInvalidMassDeleteWrongPassword() {

        $db = new MyDB();
        $UserGroup = new UserGroup();

        HelpTests::changeSessions(HelpTests::$adminId, 3, HelpTests::$adminEmail);

        $allIds = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME LIKE 'Bobby%'");
        $ids = $allIds->fetchAll(PDO::FETCH_COLUMN, 0);

        $this->assertFalse($UserGroup->delete($ids, "aS3df 4d"));

        $this->assertFalse(is_null($db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME LIKE 'Bobby%'")->fetch()));
    }

    public function testInvalidMassDeleteNotLoggedIn() {

        $db = new MyDB();
        $UserGroup = new UserGroup();

        HelpTests::endSession();

        $allIds = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME LIKE 'Bobby%'");
        $ids = $allIds->fetchAll(PDO::FETCH_COLUMN, 0);

        $this->assertFalse($UserGroup->delete($ids, "a97A65"));

        $this->assertFalse(is_null($db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME LIKE 'Bobby%'")->fetch()));
    }

    public function testValidMassDelete() {

        $db = new MyDB();
        $UserGroup = new UserGroup();

        HelpTests::changeSessions(HelpTests::$adminId, 3, HelpTests::$adminEmail);

        $allIds = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME LIKE 'Bobby%'");
        $ids = $allIds->fetchAll(PDO::FETCH_COLUMN, 0);

        $this->assertTrue($UserGroup->delete($ids, HelpTests::$adminPass));

        $this->assertFalse($db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME LIKE 'Bobby%'")->fetch());
    }

    public static function tearDownAfterClass() {

        $db = new MyDB();

        HelpTests::returnToNormal();

        $db->catchMistakes("DELETE FROM USERS WHERE USERNAME LIKE 'Bobby%'");
    }

    private function promoteFail($pass = "a97A65") {

        $db = new MyDB();
        $UserGroup = new UserGroup();

        $ids = $this->getBobbys();

        $this->assertFalse($UserGroup->promote($ids, 3, $pass));

        $allLvls = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME LIKE 'Bobby%'");
        $lvls = $allLvls->fetchAll(PDO::FETCH_COLUMN, 0);


        foreach ($lvls as $lvl) {
            $this->assertEquals(2, $lvl);
        }
    }




    private function getBobbys() {

        $db = new MyDB();

        $allIds = $db->catchMistakes("SELECT TRIM(TRAILING '@tabc.org' FROM EMAIL) FROM USERS WHERE USERNAME LIKE 'Bobby%'");
        return $allIds->fetchAll(PDO::FETCH_COLUMN, 0);
    }

}


?>