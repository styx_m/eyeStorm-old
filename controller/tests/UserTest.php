<?php


use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");

HelpTests::changeSessions(HelpTests::$adminId, 3, "BobbyA");


class UserTest extends TestCase {


    protected function setup() {

        $User = new User();
        $db = new MyDB();

        $user = $db->catchMistakes("SELECT USERNAME FROM USERS WHERE USERNAME = ? OR USERNAME = ?", ["Bobby", "newUsername"])->fetchColumn();

        $User->login($user, "aS3df 4d");
        HelpTests::changeSessions($User->getId(), 2, $user);

        return $User;
    }

    public function testCreate() {

        $User = new User();
        $this->assertEquals(0, $User->create("Bobby", "Hi Bye Bobson", "aS3df 4d", "aS3df 4d", "makeup2018@tabc.org", 2));
    }

    public function testInvalidCreateNoUsername() {

        $User = new User();
        $this->assertFalse($User->create("", "Hi Be Boson", "aS3df 4d", "aS3df 4d", "maup2018@tabc.org", 2));
    }

    public function testInvalidCreateNoLastName() {

        $User = new User();
        $this->assertFalse($User->create("Bobby", "Hi", "aS3df 4d", "aS3df 4d", "makejup2018@tabc.org", 2));
    }

    public function testInvalidCreateNoPassword() {

        $User = new User();
        $this->assertFalse($User->create("Bobby", "Hi Be Boson", "", "", "maup2018@tabc.org", 2));
    }

    public function testInvalidCreateNoPasswordMatch() {

        $User = new User();
        $this->assertFalse($User->create("Bobby", "Hi Be Boson", "aS3df 4d", "aS3dfp4d", "maup2018@tabc.org", 2));
    }

    public function testInvalidCreate0Level() {

        $User = new User();
        $this->assertFalse($User->create("Bobby", "Hi Be Boson", "aS3df 4d", "aS3df 4d", "maup2018@tabc.org", 0));
    }

    public function testTwoFactorLogin() {

        $User = new User();
        $db = new MyDB();

        $this->assertEquals(0, $User->login("Bobby", "aS3df 4d"));

        // don't automate 2fa since would need to read an email, so just pretend 2fa code input successfully
        $db->catchMistakes("UPDATE USERS SET EMAIL = ? WHERE USERNAME = ?", ["makeup2018@tabc.org", "Bobby"]);

        $this->assertTrue($User->login("Bobby", "aS3df 4d"));

        $this->assertTrue($User->isLoggedIn());
    }

    public function testCheckEmail() {

        $User = $this->setup();

        $this->assertEquals("makeup2018@tabc.org", $User->getEmail());
    }

    public function testInvalidEmailFormat() {

        $User = $this->setup();
        $this->assertFalse($User->setEmail("invalidFormat"));
    }

    public function testBadEmailDomain() {

        $User = $this->setup();

        $this->assertFalse($User->setEmail("hello2016@tabc.com"));
    }

    public function testSetTeacherEmail() {

        $User = $this->setup();

        $this->assertTrue($User->setEmail("lauren.burstein@tabc.org"));
        $this->assertEquals($User->getEmail(), ".lauren.burstein@tabc.org");
    }

    public function testMakeEmailUsable() {

        $User = $this->setup();

        $this->assertEquals($User->getEmail(),  ".lauren.burstein@tabc.org");

        $this->assertTrue($User->makeEmailUsable());

        $this->assertEquals($User->getEmail(),  "lauren.burstein@tabc.org");
    }

    public function testGetUsername() {

        $User = $this->setup();
        $this->assertEquals("Bobby", $User->getUsername());
    }

    public function testMoreThan20CharUsername() {

        $User = $this->setup();

        $this->assertFalse($User->setUsername("89dfdsfdsf89dfdsfdsfa"));
    }

    public function testEmptyUsername() {

        $User = $this->setup();

        $this->assertFalse($User->setUsername(""));
    }

    public function testSpaceInUsername() {

        $User = $this->setup();

        $this->assertFalse($User->setUsername("test space"));
    }

    public function testSetValidUsername() {

        $User = $this->setup();

        $this->assertTrue($User->setUsername("newUsername"));
        $this->assertEquals($User->getUsername(), "newUsername");
    }

    public function testGetFirstName() {

        $User = $this->setup();

        $this->assertEquals("Hi", $User->getFirstName());
    }

    public function testGetMiddleName() {

        $User = $this->setup();

        $this->assertEquals("Bye", $User->getMiddleName());
    }

    public function testGetLastName() {

        $User = $this->setup();

        $this->assertEquals("Bobson", $User->getLastName());
    }

    public function testOneWordName() {
        $User = $this->setup();

        $this->assertFalse($User->setFullName("oneWord"));
    }

    public function testFourWordName() {

        $User = $this->setup();

        $this->assertFalse($User->setFullName("coop tha cow wo"));
    }

    public function testLongInitial() {

        $User = $this->setup();

        $this->assertFalse($User->setFullName("this horrible initial"));
    }

    public function testSetValidName() {

        $User = $this->setup();

        $this->assertTrue($User->setFullName("new u. name"));

        $this->assertEquals($User->getFullname(), "new u. name");
    }

    public function testOnlyLowPassword() {

        $User = $this->setup();

        $this->assertFalse($User->setPassword("onlylower", "onlylower"));
    }

    public function testOnlyDigitPassword() {

        $User = $this->setup();

        $this->assertFalse($User->setPassword("34343434", "34343434"));
    }

    public function testNoLowerPassword() {
        $User = $this->setup();

        $this->assertFalse($User->setPassword("DSFDSF4", "DSFDSF4"));
    }

    public function testShortPassword() {

        $User = $this->setup();

        $this->assertFalse($User->setPassword("P1df", "P1df"));
    }

    public function testNotMatchingPassword() {
        $User = $this->setup();

        $this->assertFalse($User->setPassword("4Rdkl 3f", "4h 41iF"));
    }

    public function testSetValidPasswordLongNoRules() {

        $User = $this->setup();

        $this->assertTrue($User->setPassword("qwertyuiopasdfghjklz", "qwertyuiopasdfghjklz"));
    }

    public function testSetValidPassword() {

        $User = $this->setup();

        $this->assertTrue($User->setPassword("4dG dsf4G52", "4dG dsf4G52"));
    }

    public function testGetLevel() {
        $User = $this->setup();

        $this->assertEquals(2, $User->getLevel());
    }

    public function testSetLevelPast3() {

        $User = $this->setup();

        $this->assertFalse($User->setLevel(4));
    }

    public function testSetLevelZero() {

        $User = $this->setup();

        $this->assertFalse($User->setLevel(0));
    }

    /**
      *
      */
    public function testSetLevelToNaN() {

        $User = $this->setup();

        $this->expectException(TypeError::class);

        $this->assertFalse($User->setLevel("string"));
    }

    public function testSetValidLevel() {

        $User = $this->setup();
        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $User->setLevel(3);
        $this->assertEquals(3, $User->getLevel());
    }


    public function testNotificationDefaultOn() {

        $User = $this->setup();

        $this->assertEquals($User->getNotificationStatus(), 1);
    }

    public function testSetNotifications() {

        $User = $this->setup();

        $this->assertTrue($User->setNotificationStatus(false));
        $this->assertEquals($User->getNotificationStatus(), 0);
    }

    public function testDefaultTwoFactorOff() {

        $User = $this->setup();

        $this->assertEquals(0, $User->getTwoFactor());
    }

    public function testSetTwoFactor() {

        $User = $this->setup();

        $this->assertTrue($User->setTwoFactor(1));
    }

    public function testDefaultViewsZero() {

        $User = $this->setup();

        $this->assertEquals(0, $User->getViews());
    }

    public function testDefaultArticlesZero() {

        $User = $this->setup();

        $this->assertEquals(0, $User->getArticleNum());
    }

    public function testDeleteUser() {

        $User = $this->setup();

        $this->assertTrue($User->destroy($User->getId()));

        $this->assertNull($User->getEmail());
        $this->assertNull($User->getUsername());
        $this->assertNull($User->getFirstName());
        $this->assertNull($User->getLevel());
    }

    public static function tearDownAfterClass() {
        $db = new MyDB();
        $db->catchMistakes("DELETE FROM USERS WHERE USERNAME = ?", "Bobby");
    }


    private function generateArticle() {


        return '<h1>Opinion: Why Does College Matter So Much for Professional Advancement and Financial Security?</h1><h5>January 11, 2017|Ezra Friedman, Ezra Finkelstein, and Aharon Nissel</h5>'

.'<h5><img alt="" src="https://static.wixstatic.com/media/1af70b5973771d67736218fc1f7b24c8.jpg/v1/fill/w_495,h_358,al_c,q_80,usm_0.66_1.00_0.01/1af70b5973771d67736218fc1f7b24c8.jpg"></h5>'

.'<section class=\'storyContainer\'><p>With a significant pay gap between college graduates and non-college graduates, no one can deny the importance of college, as it is undoubtedly the most traditional and safest route to a well-paying job. Unfortunately, however, this safe route will take a minimum of three to four years of your life and a large chunk of money, which is likely to leave you paying off debt for years to come. Luckily, in this modern age of the internet, there are thousands of different and cheaper mediums to attain the knowledge necessary to function at high level jobs. Sadly, however, in our society where credentials are valued more than actual knowledge, this is not an option if you truly want to compete with others in certain job markets. College can be a great experience for many people, but due to an abundance offactors, the system in place today often alienates those for whom college is not an ideal option.</p>'

.'<p>While college is the clear and obvious choice for some people, for other people, it may not be the ideal choice. Some people may not be cut out for college, not specifically because of intelligence, rather, because they are simply in situations where finances take precedent over education. Imagine the scenario where a child from an inner-city went to underfunded, under-staffed, and overall just poor public schools throughout his life. By the time he is in 11th grade he may have poorer math skills than the average American student, and unless Joe Clark (from the film, Lean on Me with Morgan Freeman) himself comes to teach him, there will be little to no chance that college will be an option for this child. And, without achieving a BA, this child will ultimately not be offered the same professional opportunities as others born into more fortunate circumstances. Alas, the bigger question lingers: Why should a child who was simply unlucky enough to be born in an unfortunate neighborhood be relegated to low paying minimum wage jobs for the rest of his life? There have to be more accessible alternatives professional opportunities. where everyone, regardless of background, has an equal chance of advancing in society.</p>'


.'<p>Aside from the extreme, but all too common, scenario discussed, there are other scenarios for different people, where college may not be the ideal option. The entire formal education system does not work for everyone, and the idea of spending another four years in school taking tests can be a very daunting idea to some high school seniors. As a society we have to move away from the idea that college is the only real option if you want any sort of financial security. Because of the internet, we are blessed with so many different outlets to attain the same knowledge that students learn in college. In order to equalize the playing field for everyone, we should accept that there are many different routes, other than college, that people can take and still end up in the same, financially secure, destination.</p></section>';
    }
}


?>