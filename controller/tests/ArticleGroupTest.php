<?php


use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");



$_SERVER["HTTP_USER_AGENT"] = "hi";

class ArticleGroupTest extends TestCase {

    public static function setUpBeforeClass() {
        HelpTests::setupForTests();
    }

    public function testValidDelete() {

        $db = new MyDB();
        $ArticleGroup = new ArticleGroup();
        $Article1 = new Article();
        $Article2 = new Article();

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $ids = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE IN (?, ?) LIMIT 2", [HelpTests::$pubIssue, HelpTests::$privIssue])->fetchAll(PDO::FETCH_COLUMN, 0);

        $artInfo = $db->catchMistakes("SELECT ISSUE, URL FROM PAGEINFO WHERE ID IN (?, ?)", $ids);
        $artInfo->setFetchMode(PDO::FETCH_ASSOC);

        $fetch1 = $artInfo->fetch();
        $Article1->defineInfoFor($fetch1["ISSUE"], $fetch1["URL"]);


        $fetch2 = $artInfo->fetch();
        $Article2->defineInfoFor($fetch2["ISSUE"], $fetch2["URL"]);

        $this->assertTrue($ArticleGroup->delete($ids, HelpTests::$adminPass));

        $this->assertEquals(0, $db->catchMistakes("SELECT EXISTS(SELECT 1 FROM PAGEINFO WHERE ID = ?)", $ids[0])->fetchColumn());
        $this->assertEquals(0, $db->catchMistakes("SELECT EXISTS(SELECT 1 FROM PAGEINFO WHERE ID = ?)", $ids[1])->fetchColumn());

        // put back (doesn't matter if goes to different issue than part of, since the issue will deleted anyways as part of HelpTests)
        $Article1->create($Article1->getURL(), $Article1->getBody(), $Article1->getTags());
        $Article2->create($Article2->getURL(), $Article2->getBody(), $Article2->getTags());
    }

    public function testInvalidDeleteWrongPassword() {

        $db = new MyDB();
        $ArticleGroup = new ArticleGroup();

        $ids = $db->catchMistakes("SELECT ID FROM PAGEINFO LIMIT 2")->fetchAll(PDO::FETCH_COLUMN, 0);

        $ArticleGroup->delete($ids, "incorrect password");

        $this->assertEquals(1, $db->catchMistakes("SELECT EXISTS(SELECT 1 FROM PAGEINFO WHERE ID = ?)", $ids[0])->fetchColumn());
        $this->assertEquals(1, $db->catchMistakes("SELECT EXISTS(SELECT 1 FROM PAGEINFO WHERE ID = ?)", $ids[1])->fetchColumn());
    }

    public function testInvalidDeleteNotAuthorBadLevel() {

        HelpTests::changeSessions(null, 2, "hi");

        $db = new MyDB();
        $ArticleGroup = new ArticleGroup();

        $ids = $db->catchMistakes("SELECT ID FROM PAGEINFO LIMIT 2")->fetchAll(PDO::FETCH_COLUMN, 0);

        $ArticleGroup->delete($ids, HelpTests::$adminPass);

        $this->assertEquals(1, $db->catchMistakes("SELECT EXISTS(SELECT 1 FROM PAGEINFO WHERE ID = ?)", $ids[0])->fetchColumn());
        $this->assertEquals(1, $db->catchMistakes("SELECT EXISTS(SELECT 1 FROM PAGEINFO WHERE ID = ?)", $ids[1])->fetchColumn());

        HelpTests::changeSessions(null, 3, HelpTests::$adminEmail);
    }

    public function testValidDeleteBadLevelGoodAuthor() {

        HelpTests::changeSessions(null, 1, HelpTests::$adminEmail);

        $db = new MyDB();
        $ArticleGroup = new ArticleGroup();
        $Article1 = new Article();


        $id = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE AUTHORID = ? LIMIT 1", HelpTests::$adminId)->fetchColumn();

        $artInfo = $db->catchMistakes("SELECT ISSUE, URL FROM PAGEINFO WHERE ID = ?", $id);
        $artInfo->setFetchMode(PDO::FETCH_ASSOC);

        $fetch1 = $artInfo->fetch();
        $Article1->defineInfoFor($fetch1["ISSUE"], $fetch1["URL"]);


        $this->assertTrue($ArticleGroup->delete([$id], HelpTests::$adminPass));

        $this->assertEquals(0, $db->catchMistakes("SELECT EXISTS(SELECT 1 FROM PAGEINFO WHERE ID = ?)", $id)->fetchColumn());

        // put back where found
        $Article1->create($fetch1["URL"], $Article1->getBody(), $Article1->getTags());

        HelpTests::changeSessions(null, 3);
    }

    public function testSetTags() {

        $db = new MyDB();
        $ArticleGroup = new ArticleGroup();

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $id = $db->catchMistakes("SELECT ID FROM PAGEINFO LIMIT 1")->fetchColumn();

        $tags = $db->catchMistakes("SELECT TAG1, TAG2, TAG3 FROM TAGS WHERE ART_ID = ?", $id);

        $tags->setFetchMode(PDO::FETCH_NUM);

        $newTags = ["opinion", "reaction", "other"];
        $this->assertTrue($ArticleGroup->setMultiTags($id, $newTags, HelpTests::$adminPass));


        $newTagsDb = $db->catchMistakes("SELECT TAG1, TAG2, TAG3 FROM TAGS WHERE ART_ID = ?", $id);
        $newTagsDb->setFetchMode(PDO::FETCH_NUM);

        $this->assertEquals($newTags, $newTagsDb->fetch());

        $db->catchMistakes("UPDATE TAGS SET TAG1 = ?, TAG2 = ?, TAG3 = ? WHERE ART_ID = ?", array_merge($tags->fetch(), [$id]));
    }

    public function testSetDisplay() {

        $db = new MyDB();
        $ArticleGroup = new ArticleGroup();

        $original = $db->catchMistakes("SELECT ID, DISPLAY_ORDER FROM PAGEINFO LIMIT 1");
        $original->setFetchMode(PDO::FETCH_NUM);

        $originalInfo = $original->fetch();

        $this->assertTrue($ArticleGroup->setDisplay($originalInfo[0], HelpTests::$adminId)); // HelpTests::$adminId is random

        $this->assertEquals(HelpTests::$adminId, $db->catchMistakes("SELECT DISPLAY_ORDER FROM PAGEINFO WHERE ID = ?", $originalInfo[0])->fetchColumn());

        $db->catchMistakes("UPDATE PAGEINFO SET DISPLAY_ORDER = ? WHERE ID = ?", [$originalInfo[1], $originalInfo[0]]);
    }

    public static function tearDownAfterClass() {
        HelpTests::returnToNormal();
    }

    // FYI about previous bugs relating to UI that cannot be automated, but have been fixed
    // (as of the time it's being written at least)
    private function provideInfoPrevBugs() {

        echo "BUG: when remove all tags then add back a tag, and delete an article where tag isn't changed, undefined idx";
        echo "BUG: Making sure that all users have a profile that can be accessed
         (previously, if clicked on user's profile that did not have at least 1 article, redirect to 404";
    }

}


?>