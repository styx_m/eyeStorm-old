<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class RouterTest extends TestCase {

    public static function setUpBeforeClass() {
        HelpTests::setupForTests();
    }

    /***GET***/

    /**
      * @param $loggedIn - boolean
      * @param getFrom - issue to get
      * @param $lvl - lvl of user getting info
      *
      * @return boolean if got correct info
      */
    private function helpGetArticleGroup($loggedIn, $getFrom = null, $lvl = 3) {

        $db = new MyDB();

        $dbIssue = ($loggedIn) ?
                                   (($getFrom) ? $getFrom : HelpTests::$privIssue)
                               :    null;

        $user = HelpTests::createNewUser($lvl);

        HelpTests::helpSetSessions($user, $lvl, $loggedIn);
        $reqInfo = HelpTests::createHTTPRequest("articleGroup", ["articlesFor" => $getFrom], "GET");


        HelpTests::deleteNewUser($user["username"]);
        if (!$reqInfo) {
            return false;
        }

        $artDBQuery = $db->catchMistakes("SELECT URL, CREATED, CONCAT(F_NAME, ' ', IFNULL(M_NAME, ''), ' ', L_NAME) AS AUTHOR_NAME,
                             CONCAT(TAG1, IFNULL(CONCAT(', ', TAG2), ''), IFNULL(CONCAT(', ', TAG3), '')) AS TAGS, VIEWS, DISPLAY_ORDER,
                             PAGEINFO.ID AS ART_ID, USERNAME AS AUTHOR_USERNAME
                             FROM PAGEINFO
                             LEFT JOIN USERS
                             ON USERS.ID = AUTHORID
                             LEFT JOIN TAGS
                             ON ART_ID = PAGEINFO.ID
                             WHERE ISSUE = ?", $dbIssue);
        $artDBQuery->setFetchMode(PDO::FETCH_ASSOC);
        $artDB = $artDBQuery->fetchAll();

        $tagQuery = $db->catchMistakes("SELECT DISTINCT TAG1, TAG2, TAG3 FROM TAGS");
        $tagQuery->setFetchMode(PDO::FETCH_NUM);
        $dbTags = $tagQuery->fetchAll();
        $issueQuery = $db->catchMistakes("SELECT NAME, ISPUBLIC, NUM FROM ISSUES WHERE NUM = ?", $dbIssue);
        $maxIssue = $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES WHERE ISPUBLIC = 1 OR ?", $loggedIn)->fetchColumn();
        $issueQuery->setFetchMode(PDO::FETCH_ASSOC);

        $issueInfo = $issueQuery->fetchAll();

        $issueDB = array_merge($issueInfo[0], ["MAX" => $maxIssue]);


        foreach($reqInfo[0] as $key => $articleInfo) {
            $reqInfo[0][$key] = (array)$articleInfo;
        }

        $dbTags = array_values(array_filter(array_unique(array_merge(...$dbTags))));


        return $artDB == $reqInfo[0] && $dbTags == (array)$reqInfo[1] && $issueDB == (array)$reqInfo[2];
    }

    // should only get public
    public function testGetAllArticlesNotLoggedIn() {

        $this->assertFalse($this->helpGetArticleGroup(false));
    }

    public function testGetAllArticlesNotLevelThree() {
        $this->assertFalse($this->helpGetARticleGroup(true, HelpTests::$pubIssue, 2));
    }

    // should get all
    public function testGetAllArticlesYesLoggedIn() {

        $this->assertTrue($this->helpGetArticleGroup(true));
    }

    public function testGetAllArticlesOfPrivateIssueYesLoggedIn() {

        $this->assertTrue($this->helpGetArticleGroup(true, HelpTests::$privIssue));
    }

    public function testGetAllArticlesOfPublicIssueYesLoggedIn() {
        $this->assertTrue($this->helpGetArticleGroup(true, HelpTests::$pubIssue));
    }

    /***PUT***/

    /**
      * Helper function to test if setting tags for multiple articles works or not
      *
      * @param $tags - array of tags
      * @param $articleIds - array of article ids that should match $tags in length
      * @param $lvl - num. If 0, considered not logged in
      * @param $validPassword - boolean if should use correct password
      */
    private function helpMultiTags($tags, $articleIds, $lvl = 3, $validPassword = true, $author = false) {

        $db = new MyDB();
        $Utilities = new Utilities();

        $password = ($validPassword) ? HelpTests::$adminPass : "bad password";

        if ($author) {
            $authorId = $db->catchMistakes("SELECT AUTHORID FROM PAGEINFO WHERE ID = ? LIMIT 1", $articleIds[0])->fetchColumn();
            $username = ($authorId == HelpTests::$adminId) ? "admin" : "test1";
            HelpTests::changeSessions($authorId, $lvl, $username);
        }
        else {
            $user = HelpTests::createNewUser($lvl);
            HelpTests::helpSetSessions($user, $lvl, !!$lvl);
        }


        HelpTests::createHTTPRequest("articleGroup", [
                        "artId[]" => $articleIds,
                        "tag[]" => $tags,
                        "password" => $password],
                    "PUT");


        $marks = $Utilities->generateQuestionMarks(count($articleIds));

        $tagQuery = $db->catchMistakes("SELECT TAG1, TAG2, TAG3 FROM TAGS WHERE ART_ID IN {$marks}", $articleIds);
        $tagQuery->setFetchMode(PDO::FETCH_NUM);
        $dbTags = $tagQuery->fetchAll();
        $db->catchMistakes("UPDATE TAGS SET TAG1 = ?, TAG2 = ? WHERE ART_ID IN {$marks}",
                                 array_merge(["nonexistant", "imaginary"], $articleIds)); // reset for next tests

        HelpTests::deleteNewUser($user["username"] ?? null);

        return array_map('array_filter', $dbTags) == $tags;
    }

    public function testBadMultiTagsNotLoggedIn() {

        $this->assertFalse($this->helpMultiTags([["reaction"]], [HelpTests::$pubId], 0));
    }

    public function testBadMultiTagsBadPassword() {

        $this->assertFalse($this->helpMultiTags([["reaction"]], [HelpTests::$pubId], 3, false));
    }

    public function testBadMultiTagsNotLevelThreeNotAuthor() {

        $this->assertFalse($this->helpMultiTags([["reaction"]], [HelpTests::$pubId], 2));
    }

    public function testBadMultiTagsInvalidTags() {

        $unsafe = HelpTests::$unsafeStrings;
        array_push($unsafe, null, "");

        foreach($unsafe as $tag) {

            $this->assertFalse($this->helpMultiTags([[$tag]], [HelpTests::$pubId]));
        }
    }

    public function testBadMultiTagsMoreTagsThanArticles() {

        $this->assertFalse($this->helpMultiTags([["reaction"], ["opinion"]], [HelpTests::$pubId]));
    }

    public function testBadMultiTagsMoreArticlesThanTags() {

        $this->assertFalse($this->helpMultiTags([["reaction"]], [HelpTests::$pubId, HelpTests::$privId]));
    }

    public function testGoodMultiTagsLevelThreePrivateArticles() {

        $db = new MyDB();
        $idQuery = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? ORDER BY ID ASC LIMIT 2", HelpTests::$privIssue);
        $idQuery->setFetchMode(PDO::FETCH_COLUMN, 0);

        $this->assertTrue($this->helpMultiTags([["reaction", "feature"], ["news"]], $idQuery->fetchAll()));
    }

    public function testGoodMultiTagsAuthorPrivateArticles() {

        $this->assertTrue($this->helpMultiTags(
            [["news"]],
            [HelpTests::$privId],
            1, true, true));
    }

    public function testGoodMultiTagsLevelThreePublicArticles() {

        $db = new MyDB();
        $pubIdsQuery = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ?", HelpTests::$pubIssue);
        $pubIdsQuery->setFetchMode(PDO::FETCH_COLUMN, 0);
        $pubIds = $pubIdsQuery->fetchAll();


        $this->assertTrue($this->helpMultiTags([
                ["news", "opinion"],
                ["reaction", "other", "feature"],
                ["feature"]
            ],
            $pubIds));
    }

    public function testGoodMultiTagsAuthorPublicArticles() {

        $this->assertTrue($this->helpMultiTags([["reaction"]],
               [HelpTests::$pubId], 1, true, true));
    }

    public function testGoodMultiTagsPublicAndPrivateArticles() {

        $this->assertTrue($this->helpMultiTags([["reaction"], ["opinion", "reaction"]],
               [HelpTests::$pubId, HelpTests::$privId], 1, true, true));
    }

    /**
      * Checks if display order was changed by sending PUT to articleGroup
      *
      * @param $displayOrder - number
      * @param $status - boolean public or private
      * @param $lvl - lvl of user changing order
      * @param $loggedIn - boolean
      * @param $validPassword - boolean
      *
      * @return boolean if display order was changed or not
      */
    private function helpTestDisplayOrder(array $displayOrder, array $status, $lvl = 3, $loggedIn = true, $validPassword = true) {

        $db = new MyDB();
        $password = ($validPassword) ? HelpTests::$adminPass : "bad password";
        $ids = [];

        if ($status) {
            $ids[] = HelpTests::$pubId;
        }
        // not if...else so can have priv and pub in same api call
        if (!$status) {
            $ids[] = HelpTests::$privId;
        }

        HelpTests::createHTTPRequest("articleGroup", [
            "artId[]" => $ids,
            "order[]" => $displayOrder,
            "password" => $password
        ],
        "PUT");

        $Utilities = new Utilities();
        $marks = $Utilities->generateQuestionMarks(count($ids));

        $orderQuery = $db->catchMistakes("SELECT DISPLAY_ORDER FROM PAGEINFO WHERE ID IN {$marks}", $ids);
        $orderQuery->setFetchMode(PDO::FETCH_COLUMN, 0);
        $order = $orderQuery->fetchAll();

        $db->catchMistakes("UPDATE PAGEINFO SET DISPLAY_ORDER = 0 WHERE ID IN {$marks}", $ids);

        return $order == $displayOrder;
    }

    public function testBadChangeDisplayOrderNotLoggedIn() {

        $this->assertFalse($this->helpTestDisplayOrder([1], [1], 1, false));
    }

    public function testBadChangeDisplayOrderNotLevelThree() {

        $this->assertFalse($this->helpTestDisplayOrder([1], [1], 2));
    }
    public function testBadChangeDisplayOrderBadPassword() {

        $this->assertFalse($this->helpTestDisplayOrder([1], [1], 3, true, false));
    }

    public function testGoodChangeDisplayOrderLevelThree() {

        $this->assertFalse($this->helpTestDisplayOrder([1], [1], 3));
    }


    /***DELETE***/

    /**
      * Tries to delete articles through api
      *
      * @param $artIds - ids to delete
      * @param $user - array ["username" => $username, "id" => $id]
      * @param $lvl - lvl use should be
      * @param $validPassword - boolean if should use correct password
      * @param $loggedIn - boolean
      *
      * @return number of articles that were not deleted
      */
    private function helpDelete(array $artIds, array $user, $lvl = 3, $validPassword = true, $loggedIn = true) {

        $db = new MyDB();
        $Utilities = new Utilities();
        HelpTests::helpSetSessions($user, $lvl, $loggedIn);
        $password = ($validPassword) ? HelpTests::$adminPass : "bad password";

        HelpTests::createHTTPRequest("articleGroup",
                                        ["delArt[]" => $artIds,
                                        "password" => $password],
                                    "DELETE");

        $marks = $Utilities->generateQuestionMarks(count($artIds));

        $amountDeleted = $db->catchMistakes("SELECT COUNT(ID) FROM PAGEINFO WHERE ID IN {$marks}", $artIds)->fetchColumn();

        $db->catchMistakes("DELETE FROM PAGEINFO WHERE URL LIKE 'testGroupDelete%' OR ID IN {$marks}", $artIds);

        return $amountDeleted;
    }

    /**
      * Creates articles
      *
      * @param $status - array where 1 is public, 0 is private
      * @param $author - id of author of articles, same length as $staus
      *
      * @return array of ids of articles
      */
    private function createArts(array $status, array $author) {

        $db = new MyDB();

        for ($i = 0; $i < count($status); $i++) {

            $issue = ($status) ? HelpTests::$pubIssue : HelpTests::$privIssue;

            $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [$issue, "testGroupDelete{$i}", HelpTests::$lede, HelpTests::$body, json_encode([]),
                           $author[$i], json_encode([])]);
        }

        $idQuery = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE URL LIKE 'testGroupDelete%'");
        $idQuery->setFetchMode(PDO::FETCH_NUM);

        return $idQuery->fetchAll()[0];
    }

    public function testBadDeleteNotLoggedIn() {

        $ids = $this->createArts([1], [HelpTests::$test1Id]);

        $this->assertEquals(1, $this->helpDelete($ids, [HelpTests::$test1Id], 1, true, false));
    }

    public function testBadDeleteNotLevelThreeNotAuthor() {

        $ids = $this->createArts([1], [HelpTests::$test1Id]);

        $this->assertEquals(1, $this->helpDelete($ids, ["id"=>HelpTests::$adminId, "username"=>"admin"], 2));
    }

    public function testBadDeleteBadPassword() {

        $ids = $this->createArts([1], [HelpTests::$test1Id]);
        $this->assertEquals(1, $this->helpDelete($ids, ["id"=>HelpTests::$test1Id, "username"=>"test1"], 3, false));
    }

    public function testBadDeletePartAuthorPartNot() {

        $ids = $this->createArts([1, 1], [HelpTests::$test1Id, HelpTests::$adminId]);
        $this->assertEquals(1, $this->helpDelete($ids, ["id"=>HelpTests::$test1Id, "username"=>"test1"], 1, false));
    }

    public function testGoodDeleteOneArticle() {

        $ids = $this->createArts([1], [HelpTests::$adminId]);
        $this->assertEquals(0, $this->helpDelete($ids, ["id"=>HelpTests::$adminId, "username"=>"admin"], 3));
    }

    public function testGoodDeleteLevelThree() {

        $ids = $this->createArts([1, 0], [HelpTests::$test1Id, HelpTests::$test1Id]);
        $this->assertEquals(0, $this->helpDelete($ids, ["id"=>HelpTests::$adminId, "username"=>"admin"], 3));
    }

    public function testGoodDeleteAuthor() {

        $ids = $this->createArts([1, 0], [HelpTests::$test1Id, HelpTests::$test1Id]);
        $this->assertEquals(0, $this->helpDelete($ids, ["id"=>HelpTests::$test1Id, "username"=>"test1"], 1));
    }


    public static function tearDownAfterClass() {
        HelpTests::returnToNormal();
    }
}
?>