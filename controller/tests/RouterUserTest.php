<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class RouterUserTest extends TestCase {

    public static function setUpBeforeClass() {
        HelpTests::setupForTests();
    }


    /**
      * Gets correct info that is expected of certain tests in this class
      *
      * @param $loggedIn - boolean if info should return info as if user is logged in or not
      * @param $id - id of user whose info should be retrieved
      * @param $getMutableInfo - if should get info that can change, such as notification status, 2fa, etc
      */
    private function getBasicInfo($loggedIn, $id, $getMutableInfo = false) {

        $db = new MyDB();
        $User = new User();
        $cookieId = $User->getJWT()->id;

        $basicArticleInfoQuery = $db->catchMistakes("SELECT URL, CREATED, CONCAT(TAG1, IFNULL(CONCAT(', ', TAG2), ''),
                    IFNULL(CONCAT(', ', TAG3), '')) AS TAGS, VIEWS,
                    PAGEINFO.ID AS ART_ID, ISSUE
                    FROM PAGEINFO
                    LEFT JOIN ISSUES
                    ON PAGEINFO.ISSUE = ISSUES.NUM
                    JOIN TAGS
                    ON TAGS.ART_ID = PAGEINFO.ID
                    WHERE AUTHORID = ? AND (ISSUES.ISPUBLIC = 1 OR ?)
                    ORDER BY ISSUE, URL", [$id, $loggedIn]);

        $basicUserInfoQuery = $db->catchMistakes('SELECT IF(?, USERNAME, NULL) AS USERNAME, CONCAT(F_NAME, " ", IFNULL(M_NAME, ""), " ", L_NAME) AS NAME, LEVEL,
                                                  (
                                                      SELECT COUNT(ID) FROM PAGEINFO WHERE AUTHORID = ? AND ISSUE IN (
                                                          SELECT NUM FROM ISSUES WHERE ISPUBLIC = 1 OR ?
                                                      )
                                                  ) AS ARTICLES,
                                                  (SELECT IFNULL(SUM(VIEWS), 0) FROM PAGEINFO WHERE AUTHORID = ?) AS VIEWS
                                                  FROM USERS
                                                  WHERE ID = ?', [$id == $cookieId, $id, $loggedIn, $id, $id]);


        $basicArticleInfoQuery->setFetchMode(PDO::FETCH_OBJ);
        $basicUserInfoQuery->setFetchMode(PDO::FETCH_OBJ);

        $basicArticleInfo = $basicArticleInfoQuery->fetchAll();
        $basicUserInfo = $basicUserInfoQuery->fetchAll();

        array_push($basicUserInfo, $basicArticleInfo);


        if ($getMutableInfo) {

            $mutableInfoQuery = $db->catchMistakes("SELECT NOTIFICATIONS AS notificationStatus,
                                                    TWO_FA_ENABLED AS twoFactor, EMAIL AS email,
                                                    IF(LEFT(EMAIL, 1) = '.', 0, 1) AS emailConfirmed, ID AS id
                                                    FROM USERS
                                                    WHERE ID = ?", $id);
            $mutableInfoQuery->setFetchMode(PDO::FETCH_OBJ);
            array_push($basicUserInfo, $mutableInfoQuery->fetchAll()[0]);
        }

        return $basicUserInfo;
    }


    /*****************User***************************/

    /***GET***/

    // should only get basic, immutable info and public articles
    public function testGetUserNotLoggedIn() {

        HelpTests::endSession();

        $response = HelpTests::createHTTPRequest("user", ["user"=>"admin"], "GET");

        $this->assertEquals(2, count($response));

        $this->assertEquals($this->getBasicInfo(0, HelpTests::$adminId), $response);
    }

    // should get immutable info and both public and private articles
    public function testGetUserLoggedIn() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $response = HelpTests::createHTTPRequest("user", ["user"=>"test1"], "GET");

        $this->assertEquals($this->getBasicInfo(1, HelpTests::$test1Id), $response);
    }

    // should get all immutable, mutable, public and private articles
    public function testGetAllOwnInfo() {

        HelpTests::changeSessions(HelpTests::$test1Id, 3, "test1");

        $response = HelpTests::createHTTPRequest("user", ["user"=>"test1"], "GET");

        $this->assertEquals($this->getBasicInfo(1, HelpTests::$test1Id, true), $response);
    }

    public function testGetInfoFromUserWithoutArticles() {

        $db = new MyDB();

        HelpTests::changeSessions(HelpTests::$test1Id, 1, "test1");

        $randomNumber = 234055920489;

        $db->catchMistakes("UPDATE PAGEINFO SET AUTHORID = ? WHERE AUTHORID = ?", [$randomNumber, HelpTests::$test1Id]);

        $response = HelpTests::createHTTPRequest("user", ["user"=>"test1"], "GET");

        $this->assertEquals($this->getBasicInfo(1, HelpTests::$test1Id, true), $response);

        $db->catchMistakes("UPDATE PAGEINFO SET AUTHORID = ? WHERE AUTHORID = ?", [HelpTests::$test1Id, $randomNumber]);
    }

    public function testGetInfoFromNonexistantUser() {

        $res = HelpTests::createHTTPRequest("user", ["user"=>"sdjfaibv"], "GET");
        $this->assertEquals(404, $res);
    }

    /***PUT***/
    public function testUpdateUserNullPassword() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $res = HelpTests::createHTTPRequest("user", ["user"=>"admin", "userEmail"=>HelpTests::$adminEmail,
                                                 "2fa"=>'0', "notifications"=>'0', "password"=> null],
                                                 "PUT");

        $basic = $this->getBasicInfo(0, HelpTests::$adminId, 1);

         $this->assertEquals(1, $basic[2]->notificationStatus); // default notification status is 1
         $this->assertEquals(0, $basic[2]->twoFactor); // default 2fa is 0
    }

    public function testUpdateUserBadPassword() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $res = HelpTests::createHTTPRequest("user", ["user"=>"admin", "userEmail"=>HelpTests::$adminEmail,
                                                 "2fa"=>'0', "notifications"=>'0', "password"=> "bad password"],
                                                 "PUT");

        $basic = $this->getBasicInfo(0, HelpTests::$adminId, 1);

         $this->assertEquals(1, $basic[2]->notificationStatus); // default notification status is 1
         $this->assertEquals(0, $basic[2]->twoFactor); // default 2fa is 0
    }

    public function testUpdateUserGoodPassword() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $res = HelpTests::createHTTPRequest("user", ["user"=>"admin", "userEmail"=>HelpTests::$adminEmail,
                                                 "2fa"=>'0', "notifications"=>'0', "password"=> HelpTests::$adminPass],
                                                 "PUT");

        $basic = $this->getBasicInfo(0, HelpTests::$adminId, 1);

         $this->assertEquals(0, $basic[2]->notificationStatus);
         $this->assertEquals(0, $basic[2]->twoFactor);
    }

    // login tests

    public function testGoodLogin() {

        HelpTests::endSession();
        $User = new User();

        $res = HelpTests::createHTTPRequest("userStatus", ["username" => "admin", "password" => HelpTests::$adminPass], "PUT");

        $this->assertEquals(1, $res);
    }

    public function testBadLoginBadUsername() {

        HelpTests::endSession();
        $User = new User();

        $res = HelpTests::createHTTPRequest("userStatus", ["username" => bin2hex(random_bytes(7)), "password" => HelpTests::$adminPass], "PUT");

        $this->assertEquals(400, $res);
        $this->assertFalse($User->isLoggedIn());
    }

    public function testBadLoginBadPassword() {

        HelpTests::endSession();
        $User = new User();

        $res = HelpTests::createHTTPRequest("userStatus", ["username" => "admin", "password" => "bad".HelpTests::$adminPass], "PUT");

        $this->assertEquals(400, $res);
        $this->assertFalse($User->isLoggedIn());
    }

    public function testBadLoginNoUsername() {

        HelpTests::endSession();
        $User = new User();

        $res = HelpTests::createHTTPRequest("userStatus", ["password" => HelpTests::$adminPass], "PUT");

        $this->assertEquals(422, $res);
        $this->assertFalse($User->isLoggedIn());
    }

    public function testBadLoginNoPassword() {

        HelpTests::endSession();
        $User = new User();

        $res = HelpTests::createHTTPRequest("userStatus", ["username" => "admin"], "PUT");

        $this->assertEquals(422, $res);
        $this->assertFalse($User->isLoggedIn());
    }

    

    /***POST***/

    // make sure anyone, even if not logged in, can use api to create an account
    public function testCreateLevel1UserNotLoggedIn() {

        HelpTests::endSession();

        $this->assertTrue(HelpTests::createUser(1));
    }

    // loops through levels that someone who's creating an account while not logged in cannot have
    public function testCreateBadLevelUserNotLoggedIn() {

        HelpTests::endSession();

        $badLevels = [2, null, -1, 0, "a", NAN];

        foreach ($badLevels as $lvl) {

            $this->assertFalse(HelpTests::createUser($lvl));
        }
    }

    public function testCreateBadLevelYesLoggedIn() {

        $db = new MyDB();
        HelpTests::changeSessions(HelpTests::$test1Id, 2, "test1");

        $badLevels = [3, null, -1, 0, "a", NAN];

        foreach ($badLevels as $lvl) {

            $this->assertFalse(HelpTests::createUser($lvl));
        }

    }

    public function testCreateGoodLevelYesLoggedIn() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $db = new MyDB();

        foreach ([1, 2, 3] as $lvl) {

            $this->assertTrue(HelpTests::createUser($lvl));
        }
    }

    /***DELETE***/
    public function testGoodDeleteOwnAccount() {

        $db = new MyDB();

        foreach([1, 2, 3] as $lvl) {

            $id = HelpTests::createNewUser($lvl)["id"];

            HelpTests::changeSessions($id, $lvl, "test1");

            $res = HelpTests::createHTTPRequest("user", ["delAcc" => $id,
                                                    "password" => HelpTests::$adminPass],
                                            "DELETE");

            $userExists = !!$db->catchMistakes("SELECT ID FROM USERS WHERE ID = ?", $id)->fetchColumn();

            $this->assertFalse($userExists);
            HelpTests::deleteNewUser(); // just in case
        }
    }

    // logged in lvl 1 can't delete anyone besides self
    public function testBadDeleteOthersAccount() {

        $db = new MyDB();

        foreach([1, 2, 3, 4] as $lvl) {

            $id = HelpTests::createNewUser($lvl)["id"];

            HelpTests::changeSessions(HelpTests::$test1Id, 1, "test1");

            $res = HelpTests::createHTTPRequest("user", ["delAcc" => $id,
                                                    "password" => HelpTests::$adminPass],
                                            "DELETE");

            $userExists = !!$db->catchMistakes("SELECT ID FROM USERS WHERE ID = ?", $id)->fetchColumn();

            $this->assertTrue($userExists);
            HelpTests::deleteNewUser();
        }
    }

    // logged in lvl 2 and 3 can delete those lower than them
    public function testGoodDeleteLevelTwoAbove() {

        $db = new MyDB();

        foreach ([2, 3] as $lvl) {

            $id = HelpTests::createNewUser($lvl - 1)["id"];

            HelpTests::changeSessions(HelpTests::$test1Id, $lvl, "test1");

            $res = HelpTests::createHTTPRequest("user", ["delAcc" => $id,
                                                    "password" => HelpTests::$adminPass],
                                            "DELETE");

            $userExists = !!$db->catchMistakes("SELECT ID FROM USERS WHERE ID = ?", $id)->fetchColumn();

            $this->assertFalse($userExists);
            HelpTests::deleteNewUser();
        }
    }


    public static function tearDownAfterClass() {
        HelpTests::returnToNormal();
    }

}




?>