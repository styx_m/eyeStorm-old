<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");



class IssueTest extends TestCase {


    public static function setUpBeforeClass() {

        HelpTests::setupForTests();
    }

    public function testGetArticlesFromNotLoggedIn() {

        HelpTests::endSession();

        $Info = new Info();

        $userInfo = $Info->getArticlesFrom("admin");

        $this->assertEquals(2, count($userInfo));
    }

    public function testGetArticlesFromLoggedIn() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();

        $userInfo = $Info->getArticlesFrom("admin");

        $this->assertEquals(4, count($userInfo));
    }

    public function testGetLedesFromIssueNotLoggedInNoIssue() {

        HelpTests::endSession();

        $Info = new Info();

        $this->expectException(TypeError::class);
        $ledeInfo = $Info->getLedesFromIssue(null);

        $this->assertEquals(HelpTests::$pubIssue, $ledeInfo[0]["ISSUE"]);
    }

    public function testGetLedesFromIssueNotLoggedInRequestPrivGetPub() {

        HelpTests::endSession();

        $Info = new Info();

        $this->assertEquals(HelpTests::$pubIssue , $Info->getLedesFromIssue(HelpTests::$privIssue)[0]["ISSUE"]);
    }

    public function testGetLedesFromIssueLoggedInNoIssue() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();

        $userInfo = $Info->getLedesFromIssue();

        $this->assertEquals(HelpTests::$privIssue, $userInfo[0]["ISSUE"]);
    }

    public function testGetLedesFromIssueLoggedInCanAccessPreviousPublicIssues() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();

        $userInfo = $Info->getLedesFromIssue(HelpTests::$privIssue);

        $this->assertEquals(HelpTests::$privIssue, $userInfo[0]["ISSUE"]);
    }

    public function testGetLedesByTagsNotLoggedIn() {

        HelpTests::endSession();

        $tags = ['sports', 'reaction', 'politics', 'features'];
        $getTag = $tags[rand(0, count($tags) - 1)];

        $db = new MyDB();
        $Info = new Info();

        $countPubl = $db->catchMistakes("SELECT COUNT(PAGEINFO.ID)
                                         FROM PAGEINFO
                                         LEFT JOIN TAGS
                                         ON ? IN (TAG1, TAG2, TAG3)
                                         LEFT JOIN ISSUES
                                         ON PAGEINFO.ISSUE = ISSUES.NUM
                                         WHERE PAGEINFO.ID = ART_ID AND ISPUBLIC = ?", [$getTag, 1])->fetchColumn();

       $userInfo = $Info->getLedesFromIssue($getTag);

       $this->assertEquals($countPubl, count($userInfo));
    }

    public function testGetLedesByTagLoggedIn() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $tags = ['sports', 'reaction', 'politics', 'features'];
        $getTag = $tags[rand(0, count($tags) - 1)];

        $db = new MyDB();
        $Info = new Info();

        $countArtNum = $db->catchMistakes("SELECT COUNT(PAGEINFO.ID)
                                         FROM PAGEINFO
                                         LEFT JOIN TAGS
                                         ON ? IN (TAG1, TAG2, TAG3)
                                         WHERE PAGEINFO.ID = ART_ID", [$getTag])->fetchColumn();

       $userInfo = $Info->getLedesFromIssue($getTag);

       $this->assertEquals($countArtNum, count($userInfo));

    }

    public function testGetPageInfoBadNotLoggedIn() {

        HelpTests::endSession();

        $Info = new Info();

        $this->assertFalse($Info->getPageInfo());
    }

    public function testGetPageInfoBadLevelTwo() {

        HelpTests::changeSessions(HelpTests::$adminId, 2, "admin");

        $Info = new Info();

        $this->assertFalse($Info->getPageInfo());
    }

    public function testGetPageInfoGoodLevelThree() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();

        $this->assertTrue(is_array($Info->getPageInfo()));
    }

    public function testGetPageInfoCorrectNumOfArticles() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();
        $db = new MyDB();

        $articlesExpected = $db->catchMistakes("SELECT COUNT(ID) FROM PAGEINFO WHERE ISSUE = (SELECT MAX(NUM) FROM ISSUES)")->fetchColumn();

        $userInfo = $Info->getPageInfo();

        $this->assertEquals($articlesExpected, count($userInfo[0]));
    }

    public function testGetPageInfoCorrectPageinfoInfoGoodLevelThree() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();
        $db = new MyDB();

        $keysToTest = ["URL", "VIEWS", "DISPLAY_ORDER", "CREATED"];

        $artInfo = $Info->getPageInfo();

        // random tests on results
        for ($i = 0; $i < 5; $i++) {

           $key = $keysToTest[rand(0, count($keysToTest) - 1)];

           $partToTest = $artInfo[0][$i % count($artInfo[0])];

           $dbVal = $db->catchMistakes("SELECT {$key} FROM PAGEINFO WHERE ID = ?", [$partToTest["ART_ID"]])->fetchColumn();

           $this->assertEquals($dbVal, $partToTest[$key]);
        }
    }

    public function testGetPageInfoCorrectAuthorInfoGoodLevelThree() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();
        $db = new MyDB();

        $keysToTest = ["AUTHOR_USERNAME" => "USERNAME", "AUTHOR_NAME" => "CONCAT(F_NAME, ' ', IFNULL(M_NAME, ''), ' ', L_NAME)"];

        $artInfo = $Info->getPageInfo();

        // random tests on results
        for ($i = 0; $i < 5; $i++) {

           $key = array_rand($keysToTest);

           $partToTest = $artInfo[0][$i % count($artInfo[0])];

           $dbVal = $db->catchMistakes("SELECT {$keysToTest[$key]} FROM USERS WHERE USERNAME = ?", [$partToTest["AUTHOR_USERNAME"]])->fetchColumn();

           $this->assertEquals($dbVal, $partToTest[$key]);
        }
    }

    public function testGetPageInfoCorrectIssueInfo() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();
        $db = new MyDB();

        $artInfo = $Info->getPageInfo();

        $issueQuery = $db->catchMistakes("SELECT NAME, ISPUBLIC, NUM, MAX(NUM) AS MAX FROM ISSUES WHERE NUM = (SELECT MAX(NUM) FROM ISSUES)");
        $issueQuery->setFetchMode(PDO::FETCH_ASSOC);
        $issueInfo = $issueQuery->fetchAll();

        $this->assertTrue($issueInfo[0] == $artInfo[2]);
    }

    public function testGetPageInfoPastPublicCorrectIssueInfo() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Info = new Info();
        $db = new MyDB();

        $artInfo = $Info->getPageInfo(HelpTests::$pubIssue);

        $issueQuery = $db->catchMistakes("SELECT ISSUES.NAME AS NAME, ISSUES.ISPUBLIC AS ISPUBLIC, ISSUES.NUM AS NUM, MAX(UNRESTRAINED.NUM) AS MAX FROM ISSUES
                                          LEFT JOIN ISSUES AS UNRESTRAINED
                                          ON 1
                                          WHERE ISSUES.NUM = ?", HelpTests::$pubIssue);

        $issueQuery->setFetchMode(PDO::FETCH_ASSOC);
        $issueInfo = $issueQuery->fetchAll();

        $this->assertTrue($issueInfo[0] == $artInfo[2]);
    }


    public function testGetUsersGoodCountInfoNotLoggedIn() {

        HelpTests::endSession();

        $Info = new Info();

        $userInfo = $Info->getUsersInfo();

        $this->assertEquals(5, count($userInfo[0]));
    }

    public function testGetUsersGoodCountUsers() {

        HelpTests::endSession();

        $Info = new Info();
        $db = new MyDB();

        $userCount = $db->catchMistakes("SELECT COUNT(ID) FROM USERS")->fetchColumn();

        $userInfo = $Info->getUsersInfo();

        $this->assertEquals($userCount, count($userInfo));
    }

    public function testGetIssuesNotLoggedIn() {

        HelpTests::endSession();

        $Info = new Info();
        $db = new MyDB();

        $info = $db->catchMistakes("SELECT NUM, IFNULL(NAME, 'N/A') AS NAME, IFNULL(SUM(VIEWS), 0) AS VIEWS, MADEPUB FROM ISSUES
                                            LEFT JOIN PAGEINFO
                                            ON NUM = ISSUE
                                            WHERE ISPUBLIC = ?
                                            GROUP BY NUM
                                            ORDER BY NUM DESC", 1);
        $info->setFetchMode(PDO::FETCH_ASSOC);
        $dbIssueInfo = $info->fetchAll();

        $this->assertEquals($dbIssueInfo, $Info->getIssues());
    }

    public function testGetIssuesLoggedIn() {

        HelpTests::endSession();

        $Info = new Info();
        $db = new MyDB();

        $info = $db->catchMistakes("SELECT NUM, IFNULL(NAME, 'N/A') AS NAME, IFNULL(SUM(VIEWS), 0) AS VIEWS, MADEPUB FROM ISSUES
                                            LEFT JOIN PAGEINFO
                                            ON NUM = ISSUE
                                            WHERE ISPUBLIC = ? OR ?
                                            GROUP BY NUM
                                            ORDER BY NUM DESC", [1, 0]);
        $info->setFetchMode(PDO::FETCH_ASSOC);
        $dbIssueInfo = $info->fetchAll();

        $this->assertEquals($dbIssueInfo, $Info->getIssues());
    }


    public static function tearDownAfterClass() {

        HelpTests::returnToNormal();
    }


}




?>