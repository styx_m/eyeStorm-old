<?php





use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class IssueTest extends TestCase {


    public static function setUpBeforeClass() {

        HelpTests::setupForTests();
    }

    public function testMaxPrivateIssue() {

        $Issue = new Issue();
        $db = new MyDB();

        $this->assertEquals(HelpTests::$privIssue, $Issue->getMax());
    }

    public function testMaxPublicIssue() {

        $Issue = new Issue();
        $db = new MyDB();

        $this->assertEquals(HelpTests::$pubIssue, $Issue->getMax(true));
    }

    public function testSetName() {

        $Issue = new Issue();
        $db = new MyDB();


        $Issue->defineInfoFor(HelpTests::$privIssue);

        $oldName = $Issue->getName();

        $this->assertTrue($Issue->setName("TE"));


        $this->assertEquals($Issue->getName(), "TE");

        $this->assertTrue($Issue->setName($oldName));

        $Issue->__destruct();
    }

    public function testCreateIssue() {

        $Issue = new Issue();
        $db = new MyDB();

        $maxIssue = $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES")->fetchColumn();

        $Issue->create();

        $this->assertEquals($Issue->getMax(), $maxIssue + 1); // make sure last test's result was fully removed

        $Issue->__destruct();
    }

    public function testValidSetPublic() {

        $Issue = new Issue();
        $db = new MyDB();

        $Issue->defineInfoFor(HelpTests::$privIssue);

        $name = $db->catchMistakes("SELECT NAME FROM ISSUES WHERE NUM = ?", HelpTests::$privIssue)->fetchColumn();
        $db->catchMistakes("UPDATE ISSUES SET NAME = ? WHERE NUM = ?", ["hello", HelpTests::$privIssue]);

        $this->assertEquals(0, $db->catchMistakes("SELECT ISPUBLIC FROM ISSUES WHERE NUM = ?", HelpTests::$privIssue)->fetchColumn());

        $this->assertTrue($Issue->setPublic());

        $this->assertTrue($Issue->getPublic());

        $this->assertEquals(date("Y-m-d"), $Issue->getMadePub());

        $db->catchMistakes("UPDATE ISSUES SET NAME = ? WHERE NUM = ?", [$name, HelpTests::$privIssue]);
    }

    public function testInvalidSetPublicNoName() {

        $db = new MyDB();
        $Issue = new Issue();

        $name = $db->catchMistakes("SELECT NAME FROM ISSUES WHERE NUM = ?", HelpTests::$privIssue)->fetchColumn();
        $db->catchMistakes("UPDATE ISSUES SET NAME = ? WHERE NUM = ?", [null, HelpTests::$privIssue]);

        $this->assertFalse($Issue->setPublic());

        $db->catchMistakes("UPDATE ISSUES SET ISPUBLIC = ?, NAME = ? WHERE NUM = ?", [0, $name, HelpTests::$privIssue]);
    }

    public static function tearDownAfterClass() {

        HelpTests::returnToNormal();
    }


}



?>