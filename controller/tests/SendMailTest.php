<?php


use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class SendMailTest extends TestCase {

    public static function setUpBeforeClass() {

        HelpTests::setupForTests();
    }

    public function testSendMail() {

        $sendMail = new SendMail();

        $this->assertTrue($sendMail->toLevel(3));
    }
    
    public static function tearDownAfterClass() {

        HelpTests::returnToNormal();
    }


}




?>