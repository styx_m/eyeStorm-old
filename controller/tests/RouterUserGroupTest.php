<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class RouterUserGroupTest extends TestCase {

    public static function setUpBeforeClass() {
        HelpTests::setupForTests();
    }


    /******************UserGroup*****************/

    /***GET***/

    // if not logged in, can't get certain info about users that others will (such as level)
    public function testGetAllUsersWhileNotLoggedIn() {

        $db = new MyDB();
        HelpTests::endSession();

        $res = HelpTests::createHTTPRequest("userGroup", [], "GET");

        $userInfoForNotLoggedInUsers = ["NAME", "ARTICLES", "VIEWS", "ID"];

        $numOfUsers = $db->catchMistakes("SELECT COUNT(ID) FROM USERS")->fetchColumn();

        $this->assertEquals($numOfUsers, count($res)); // verify got all users

        $this->assertEquals($userInfoForNotLoggedInUsers, array_keys((array)$res[0]));
    }

    public function testGetAllUsersWhileYesLoggedIn() {

        $db = new MyDB();

        foreach ([1, 2, 3] as $lvl) {

            HelpTests::changeSessions(HelpTests::$test1Id, $lvl, "test1");

            $res = HelpTests::createHTTPRequest("userGroup", [], "GET");

            $userInfoForNotLoggedInUsers = ["NAME", "USERNAME", "LEVEL", "ARTICLES", "VIEWS", "ID"];

            $numOfUsers = $db->catchMistakes("SELECT COUNT(ID) FROM USERS")->fetchColumn();

            $this->assertEquals($numOfUsers, count($res)); // verify got all users

            $this->assertEquals($userInfoForNotLoggedInUsers, array_keys((array)$res[0]));
        }
    }

    /**PUT***/

    public function testNotLoggedInPromote() {

        $db = new MyDB();

        HelpTests::endSession();

        $res = HelpTests::createHTTPRequest("userGroup", ["lvl[]" => [2],
                                               "pass"=> HelpTests::$adminPass,
                                               "name[]" => ["admin", "test1"]],
                                         "PUT");

       $levelQuery = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME IN (?, ?)", ["admin", "test1"]);

       $levelQuery->setFetchMode(PDO::FETCH_COLUMN, 0);

       $this->assertEquals([3, 1], $levelQuery->fetchAll());
    }

    // can't pro/de mote users already higher than you (lvl 2 user changing lvl 3)
    public function testBadLevelPromoteHigher() {

        $db = new MyDB();


        foreach ([1, 2, 3] as $lvl) {

            $newUser = HelpTests::createNewUser($lvl);
            $lvlAbove1 = HelpTests::createNewUser($lvl + 1, $lvl + 1 . "Above");
            $lvlAbove2 = HelpTests::createNewUser($lvl + 2, $lvl + 2 . "Above");

            $usernamesAbove = [$lvlAbove1["username"], $lvlAbove2["username"]];


            HelpTests::changeSessions($newUser["id"], $lvl, $newUser["username"]);

            $res = HelpTests::createHTTPRequest("userGroup", ["lvl[]" => [2, 2],
                                                "pass"=> HelpTests::$adminPass,
                                                "name[]" => $usernamesAbove],
                                            "PUT");

            $levelQuery = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME IN (?, ?)", $usernamesAbove);
            $levelQuery->setFetchMode(PDO::FETCH_COLUMN, 0);
            $levels = $levelQuery->fetchAll();



            $this->assertEquals([$lvl + 1, $lvl + 2], $levels);

            HelpTests::deleteNewUser($lvlAbove1["username"]);
            HelpTests::deleteNewUser($lvlAbove2["username"]);
            HelpTests::deleteNewUser($newUser["username"]);
        }
    }

    // a lvl 2 user can't promote a lvl 1 to be lvl 3
    public function testBadPromoteLowerToHigher() {

         $db = new MyDB();

        foreach ([2, 3] as $lvl) {

            $newUser = HelpTests::createNewUser($lvl);
            $userBelow = HelpTests::createNewUser($lvl - 1, "Below");

            HelpTests::changeSessions($newUser["id"], $lvl, $newUser["username"]);

            $res = HelpTests::createHTTPRequest("userGroup", ["lvl[]" => [$lvl + 1],
                                                "pass"=> HelpTests::$adminPass,
                                                "name[]" => [$userBelow["username"]]],
                                            "PUT");

            $dbLevel = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME = ?", $userBelow["username"])->fetchColumn();

            $this->assertEquals($lvl - 1, $dbLevel);

            HelpTests::deleteNewUser($userBelow["username"]);
            HelpTests::deleteNewUser($newUser["username"]);
        }

    }

    // can't have for example a lvl 2 user pro/de moting another lvl 2 user
    public function testBadPromoteUsersEqual() {

        $db = new MyDB();


        foreach ([1, 2, 3] as $lvl) {

            $newUser = HelpTests::createNewUser($lvl);
            $lvlAbove1 = HelpTests::createNewUser($lvl, "aAbove");
            $lvlAbove2 = HelpTests::createNewUser($lvl, "bAbove");

            $usernamesAbove = [$lvlAbove1["username"], $lvlAbove2["username"]];


            HelpTests::changeSessions($newUser["id"], $lvl, $newUser["username"]);

            $res = HelpTests::createHTTPRequest("userGroup", ["lvl[]" => [1, 3],
                                                "pass"=> HelpTests::$adminPass,
                                                "name[]" => $usernamesAbove],
                                            "PUT");

            $levelQuery = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME IN (?, ?)", $usernamesAbove);
            $levelQuery->setFetchMode(PDO::FETCH_COLUMN, 0);
            $levels = $levelQuery->fetchAll();

            $this->assertEquals([$lvl, $lvl], $levels);

            HelpTests::deleteNewUser($lvlAbove1["username"]);
            HelpTests::deleteNewUser($lvlAbove2["username"]);
            HelpTests::deleteNewUser($newUser["username"]);
        }
    }

    // are allowed to promote users lower than you are to your level
    public function testGoodPromoteUsersLower() {

        $db = new MyDB();

        foreach ([2, 3] as $lvl) {

            $newUser = HelpTests::createNewUser($lvl, "master");
            $lvlLess = HelpTests::createNewUser($lvl - 1, "diff");

            HelpTests::changeSessions($newUser["id"], $lvl, $newUser["username"]);

            $res = HelpTests::createHTTPRequest("userGroup", ["lvl[]" => [$lvl],
                                                "pass"=> HelpTests::$adminPass,
                                                "name[]" => [$lvlLess["username"]]],
                                            "PUT");

            $dbLevel = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME = ?", $lvlLess["username"])->fetchColumn();


            $this->assertEquals($lvl, $dbLevel);

            HelpTests::deleteNewUser($newUser["username"]);
            HelpTests::deleteNewUser($lvlLess["username"]);
        }
    }

    // are allowed to promote user lower to lower (lvl 3 can promote lvl 1 to lvl 2)
    public function testGoodPromoteUsersLowerToLower() {

        $db = new MyDB();

        $masterLvl = 3;

        $newUser = HelpTests::createNewUser($masterLvl, "master");
        $lvlLess = HelpTests::createNewUser(1, "diff");

        HelpTests::changeSessions($newUser["id"], $masterLvl, $newUser["username"]);

        $res = HelpTests::createHTTPRequest("userGroup", ["lvl[]" => [2],
                                            "pass"=> HelpTests::$adminPass,
                                            "name[]" => [$lvlLess["username"]]],
                                        "PUT");

        $dbLevel = $db->catchMistakes("SELECT LEVEL FROM USERS WHERE USERNAME = ?", $lvlLess["username"])->fetchColumn();


        $this->assertEquals(2, $dbLevel);

        HelpTests::deleteNewUser($newUser["username"]);
        HelpTests::deleteNewUser($lvlLess["username"]);
    }


    /***DELETE***/

    /**
      * Tries to delete group of users
      *
      * @param $lvl - level of user that will try to delete
      * @param $lvlOfUsersToDelete - see param name
      * @param $options - array of booleans where [0] is whether or not user should be logged in and
      *    [1] is if use should give correct password
      *
      * @return array where [0] is ids after creating users that this tried to delete,
      *    [1] is results of query to get ids after attempting to delete users created based on info given in params
      */
    private function basicDeletionSetup($lvl, $lvlOfUsersToDelete, $options = [true, true]) {

            $db = new MyDB();

            $newUser = HelpTests::createNewUser($lvl, "master");
            $lvlHigher1 = HelpTests::createNewUser($lvlOfUsersToDelete, "diff");
            $lvlHigher2 = HelpTests::createNewUser($lvlOfUsersToDelete, "diff2");

            $ids = [$lvlHigher1["id"], $lvlHigher2["id"]];

            HelpTests::helpSetSessions($newUser, $lvl, $options[0]);

            $password = ($options[1]) ? HelpTests::$adminPass : "bad password";

            $res = HelpTests::createHTTPRequest("userGroup", ["delAcc[]" => $ids,
                                                "password"=> $password],
                                            "DELETE");

            $dbIdsQuery = $db->catchMistakes("SELECT ID FROM USERS WHERE ID IN (?, ?)", $ids);
            $dbIdsQuery->setFetchMode(PDO::FETCH_COLUMN, 0);
            $dbIds = $dbIdsQuery->fetchAll();

            HelpTests::deleteNewUser($lvlHigher1["username"]);
            HelpTests::deleteNewUser($lvlHigher2["username"]);
            HelpTests::deleteNewUser($newUser["username"]);

            return [$ids, $dbIds];
    }

    public function testBadDeleteHigherLvl() {

        foreach ([1, 2] as $lvl) {

            $results = $this->basicDeletionSetup($lvl, $lvl + 1);

            $this->assertEquals($results[0], $results[1]);
        }
    }

    public function testBadDeleteEqualLvl() {

        foreach ([1, 2] as $lvl) {

            $results = $this->basicDeletionSetup($lvl, $lvl);

            $this->assertEquals($results[0], $results[1]);
        }
    }

    public function testBadDeleteNotLoggedIn() {

        foreach ([2, 3] as $lvl) {

            $results = $this->basicDeletionSetup($lvl, $lvl - 1, [false, true]);

            $this->assertEquals($results[0], $results[1]);
        }
    }

    public function testBadDeleteBadPassword() {

        foreach ([2, 3] as $lvl) {

            $results = $this->basicDeletionSetup($lvl, $lvl - 1, [true, false]);

            $this->assertEquals($results[0], $results[1]);
        }
    }

    public function testGoodDelete() {

        foreach ([2, 3] as $lvl) {

            $results = $this->basicDeletionSetup($lvl, $lvl - 1);

            $this->assertEmpty($results[1]);
        }
    }




    public static function tearDownAfterClass() {
        HelpTests::returnToNormal();
    }

}




?>